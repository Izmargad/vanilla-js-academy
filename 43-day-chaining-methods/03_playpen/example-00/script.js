/**
 *  Create a small DOM manipulation library with public methods you can use to…

    Get an array of items from the DOM.
    Get the first and last matching items from the DOM.
    Add and remove a class to all matching elements.
    
    You should be able to pass in a selector, 
    call a helper function
    on your matching elements
 */

        // Start by creating an IIFE 
        // The returned object functions gets assigned to our variable 
        // DML is a DOM Manipulation Library
        // It consists of a collection of handy dom methods 
        var _DML = (function () {

            /**
             * The constructor object
             */
            const Constructor = function (selector) {
                this.elements = Array.prototype.slice.call(document.querySelectorAll(selector, 0));
            }


            // Now we want an Immutable copy of the elements
            // Thanks for reminding me of this @Kieran Barker
            Constructor.prototype.items = function () {
                return Array.prototype.slice.call(this.elements);
            };

            // Construct an object prototype
            // This object prototype gets the first item in a set of elements
            Constructor.prototype.getFirst = function () {
                return this.elements[0]
                
            }
            // Returns first element that matches selector given when initializing the library
            // Constructor.prototype.getFirst = function () {
            //     return document.querySelector(this.selector);
            // }
            // Construct an object prototype
            // This object prototype gets the last item in a set of elements
            Constructor.prototype.getLast = function () {
                return this.elements[this.elements.length - 1]
            }

            // Construct an object prototype
            // This object prototype adds a class to every item in a set of elements
            Constructor.prototype.addClass = function (className) {
                this.elements.forEach(el => el.classList.add(className))
                return this

            }
          
            // Construct an object prototype
            // This object prototype removes a class from every item in a set of elements
            Constructor.prototype.removeClass = function (className) {
                this.elements.forEach(el => el.classList.remove(className))
                return this
            }

            return Constructor;

        })();

            const list = new _DML('li');
            const buttons = new _DML('button');
            const numList = new _DML('ol', 'li');
            const indigoButton = new _DML('#button-7');

            console.log('The buttons I could find are: ', buttons);
            console.log('The unordered list items I could find are: ', list);
            console.log('The numbered items are: ', numList);

            // Get the first matching item from the DOM
            const firstListItem = list.getFirst();
            console.log('The first list item is: ', firstListItem.textContent);

            // Get the last matching item from the DOM
            const lastListItem = list.getLast();
            console.log('I am the last item listed: ', lastListItem.textContent);

            // Remove a class from all matching elements
            buttons.removeClass('btn-blue');
            const firstButton = buttons.getFirst();
            console.log(firstButton);
            firstButton.addClass('btn-green') // expecting the first button to be green

            // Add a class from all matching elements
            //buttons.addClass('btn-purple');

            // remove a class to a specific element
            //indigoButton.removeClass('btn-purple')

            // add a class now to that element
            //indigoButton.addClass('btn-pink');

            // Remove a class from, then add a class to, all matching elements
            buttons.removeClass('btn-blue').addClass('btn-pink')