# Converting a string into an array

You can convert a string into an array by splitting it after a specific character (or characters).

In the ``split()`` method, the first argument, the delimiter, is the character or characters to split by. As an optional second argument, you can stop splitting your string after a certain number of delimiter matches have been found.

````javascript
var shoppingList = 'Soda, turkey sandwiches, potato chips, chocolate chip cookies';
var menu = shoppingList.split(', ');
var limitedMenu = shoppingList.split(', ', 2);

// logs ["Soda", "turkey sandwiches", "potato chips", "chocolate chip cookies"]
console.log(menu);

// logs ["Soda", "turkey sandwiches"]
console.log(limitedMenu);
````

## Browser Compatibility

Supported in all modern browsers, and at least back to IE6

* [code:](split.html)
