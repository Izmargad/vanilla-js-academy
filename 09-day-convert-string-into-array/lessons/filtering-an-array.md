# Filtering an array

The Array.filter() method creates a new array with only elements that pass a test you include as a callback function. The callback accepts three arguments: the current item in the loop’s value, its index, and the array itself.

````javascript
// Create a new array with only numbers greater than 10
var newArray = [1, 2, 7, 42, 99, 101].filter(function (item) {
 return item > 10;
});

// logs [42, 99, 101]
console.log(newArray);
````
