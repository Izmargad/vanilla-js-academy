# Common Issue: Keeping Code DRY

One challenge people struggle with early on is how to abstract code.

DRY is an acronym for Don’t Repeat Yourself. A common issue I see with this project is the same code repeated twice with a few small changes to account for differences in the two forms.

Typically, students will start by getting both checkboxes with the querySelector() method, and save each one to a variable.

````javascript
// Get checkboxes
var showPW = document.querySelector('#show-password');
var showPWs = document.querySelector('#show-passwords');


````

Then, they’ll attach an event listener to each one.

For the form with one password field, they’ll get the field with querySelector() and update it’s type. For the form with two fields, they’ll use querySelectorAll() to get all of the fields. Then, they’ll loop through and update each one.

````javascript
// Listen to all click events in the browser
showPW.addEventListener('click', function (event) {

 // Get the password field
 var password = document.querySelector('#password');

 // If the toggle is checked, change the type to "text"
 // Otherwise, change it back to "password"
 if (event.target.checked) {
  password.type = 'text';
 } else {
  password.type = 'password';
 }

}, false);

// Listen to all click events in the browser
showPWs.addEventListener('click', function (event) {

 // Check target password fields
 var passwords = Array.prototype.slice.call(document.querySelectorAll('#current-password, #new-password'));

 // Loop through each password field
 passwords.forEach(function (password) {

  // If the toggle is checked, change the type to "text"
  // Otherwise, change it back to "password"
  if (event.target.checked) {
   password.type = 'text';
  } else {
   password.type = 'password';
  }

 });

}, false);




````

As you can see, this effectively doubles the amount of code you have to include, and involves a lot of repeated code. It also means that you would need to copy/paste and modify the code for every form you want to use this script with.

Abstracting the code a bit by using a custom data attribute with your selectors lets you write your code once and use it on as many forms as you need.
