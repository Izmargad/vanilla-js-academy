# Project Review: Toggle Password Visibility in Multiple Forms

I started this project by modifying the markup. On each password toggle checkbox, I added a [data-pw-toggle] attribute. For each attribute, I set a value with the ID selectors for the fields that the checkbox should toggle, comma-separated.

````javascript
<input type="checkbox" name="show-passwords" id="show-password" data-pw-toggle="#password">
<!-- ... -->
<input type="checkbox" name="show-passwords" id="show-passwords" data-pw-toggle="#current-password, #new-password">

````

For this project, I used event delegation, attaching a click event listener to the document.

````javascript
// Listen to all click events in the browser
document.addEventListener('click', function (event) {
 // Stuff happens here...
}, false);



````

Next, I used the matches() method on the event.target to check if the clicked element has the [data-pw-toggle] attribute on it.

If the element does not have the attribute, I return to stop the callback function.

````javascript
// Listen to all click events in the browser
document.addEventListener('click', function (event) {

 // Check if clicked item was a password toggle
 // If not, return and stop running the callback function
 if (!event.target.matches('[data-pw-toggle]')) return;

}, false);



````

Next, I use the getAttribute() method to get the value of the [data-pw-toggle] attribute. Remember, these are the selectors for the fields that the checkbox should toggle.

I pass the value into querySelectorAll() to get all matching elements, and then into Array.prototype.slice.call() to convert it from a NodeList to an array.

````javascript
// Listen to all click events in the browser
document.addEventListener('click', function (event) {

 // Check if clicked item was a password toggle
 // If not, return and stop running the callback function
 if (!event.target.matches('[data-pw-toggle]')) return;

 // Check target password fields
 var passwords = Array.prototype.slice.call(document.querySelectorAll(event.target.getAttribute('data-pw-toggle')));

}, false);
````

Finally, I use the Array.forEach() method to loop through each of the passwords fields. For each one, I check to see if the checkbox is checked or not with the checked property.

If it’s checked, I set the type of the password field to text. If it’s not, I set it to password. This will toggle visibility to masked or visible, respectively.

````javascript
// Listen to all click events in the browser
document.addEventListener('click', function (event) {

 // Check if clicked item was a password toggle
 // If not, return and stop running the callback function
 if (!event.target.matches('[data-pw-toggle]')) return;

 // Check target password fields
 var passwords = Array.prototype.slice.call(document.querySelectorAll(event.target.getAttribute('data-pw-toggle')));

 // Loop through each password field
 passwords.forEach(function (password) {

  // If the toggle is checked, change the type to "text"
  // Otherwise, change it back to "password"
  if (event.target.checked) {
   password.type = 'text';
  } else {
   password.type = 'password';
  }

 });

}, false);
````
