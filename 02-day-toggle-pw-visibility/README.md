# Rough Notes from Slack Channel

## From the review Pin

### Question 1

**Question:** When exactly should I use variable location inside the event, or I should never do that?

**Answer:** In this particular case, it's better to save the variable globally. If you save it inside the function, then the browser has to keep getting the element over and over again every time the user the toggles the checkbox

### Question 2

**Question:** I see that Chris is using a click  element. Personally I used a change  element based on the following resources.

[Change Event](https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement/change_event)

[Click Event](https://developer.mozilla.org/en-US/docs/Web/API/Element/click_event)

If multiple event types can be used, how would you decide which event type to use best?

You should use change, since a checkbox can be changed via a keyboard (tab to element, press space). Also, it can be changed from some other Javascript code (in the future), or via a label being clicked if that label is ‘attached’ to the checkbox.

### Random Comments not quite clear

It is more performant to use *event.target* instead of running *querySelector()* again. Because the *event.target* already holds a reference to the element, while querying the DOM again is more work for the browser.

### References worth exploring

* <https://gomakethings.com/listening-for-click-events-with-vanilla-javascript/>
* <https://gomakethings.com/why-event-delegation-is-a-better-way-to-listen-for-events-in-vanilla-js/>
* <https://stackoverflow.com/a/4471462/1944>
* <https://caniuse.com/#feat=let>
* <https://caniuse.com/#feat=const>
* <https://gomakethings.com/how-to-get-started-with-web-development/>
* <https://gomakethings.com/when-should-you-add-the-defer-attribute-to-the-script-element/>

### Question 4

Is there ever a time when a professional developer would put JS at the bottom of HTML?  I always use a separate file.

### Answer 4

Re. question 2, "inlining" your JS into the HTML at the bottom is a relatively old and well used performance tactic. For instance, the venerable YSslow has been recommending this for years. However, there are newer standards we can use such as async and defer:

<https://flaviocopes.com/javascript-async-defer/>

developer.yahoo.comdeveloper.yahoo.com
Best Practices for Speeding Up Your Web Site - Yahoo Developer Network(opens in new tab)

The Exceptional Performance team has identified a number of best practices for making web pages fast.
flaviocopes.comflaviocopes.com

Efficiently load JavaScript with defer and async(opens in new tab)
When loading a script on an HTML page, you need to be careful not to harm the loading performance of the page. Depending on where and how you add your scripts to an HTML page will influence the loading time(46 kB)

* <https://gomakethings.com/inlining-literally-everything-for-better-performance/>
  
### Stuff I now know I don't know

- ternary operators: <https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Conditional_Operator>
- arrow functions

### Event Delegation

* <https://gomakethings.com/wtf-is-use-capture-in-vanilla-js-event-listeners/>
* <https://gomakethings.com/when-do-you-need-to-use-usecapture-with-addeventlistener/>
* <https://gomakethings.com/why-you-shouldnt-just-always-set-usecapture-to-true-with-vanilla-js-event-listeners/>
* <https://gomakethings.com/when-to-use-use-capture-in-your-event-listeners/>
