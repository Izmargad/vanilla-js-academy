# Common Issue: Variable Location

One common issue I see is defining persistent variables inside of the event listener instead of outside of it.

In the example below, the browser needs to run querySelector() and set new variables every time the event listener fires. But in this case, the password element is persistent and does not change from event-to-event.

````javascript
// Check the toggle checkbox
var toggle = document.querySelector('#show-password');

// Listen for click events on the toggle
toggle.addEventListener('click', function (event) {

 // Get the password field
 var password = document.querySelector('#password');

 // If the toggle is checked, change the type to "text"
 // Otherwise, change it back to "password"
 if (toggle.checked) {
  password.type = 'text';
 } else {
  password.type = 'password';
 }

}, false);

````

It’s more performant to define it outside of the event listener at the start of the script, and reference it inside the listener whenever it’s needed.

````javascript
// Get the password field and toggle checkbox
var password = document.querySelector('#password');
var toggle = document.querySelector('#show-password');

// Listen for click events on the toggle
toggle.addEventListener('click', function (event) {

 // If the toggle is checked, change the type to "text"
 // Otherwise, change it back to "password"
 if (toggle.checked) {
  password.type = 'text';
 } else {
  password.type = 'password';
 }

}, false);


````
