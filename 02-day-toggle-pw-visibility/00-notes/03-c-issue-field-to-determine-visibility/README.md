# Common Issue: Using the field to determine visibility

One of the more common ways I see people approach this challenge is by checking to see what the current visibility of the password when the checkbox is checked.

````javascript
// Listen for click events on the toggle
toggle.addEventListener('click', function (event) {

 // If the password is hidden, show it
 // Otherwise, hide it
 if (password.type === 'password') {
  password.type = 'text';
 } else {
  password.type = 'password';
 }

}, false);


````

This works, but it’s possible for the checkbox and password field to get out of sync.

Imagine if another script somewhere in your codebase changed the type on your password field back to password, but the checkbox to show visibility was still checked.

The next time you click it, it will become unchecked, indicating that the password should be hidden again. But because the password is already hidden, your script will show it.

Tying the visibility to the checkbox state ensures that the expectation and the behavior remain in sync.
