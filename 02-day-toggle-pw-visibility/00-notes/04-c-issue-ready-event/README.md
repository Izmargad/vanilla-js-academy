# Common Issue: Ready Event

I often see developers who come from a jQuery background use a vanilla JS equivalent of the jQuery.ready() event before running their code.

````javascript
document.addEventListener('load', function () {

 // Get the password field and toggle checkbox
 var password = document.querySelector('#password');
 var toggle = document.querySelector('#show-password');

 // Listen for click events on the toggle
 toggle.addEventListener('click', function (event) {

  // If the toggle is checked, change the type to "text"
  // Otherwise, change it back to "password"
  if (toggle.checked) {
   password.type = 'text';
  } else {
   password.type = 'password';
  }

 }, false);

}, false);


````

While this works, you can achieve the same effect without the load event listener by putting your code in the footer. It’s better for performance, too.

````javascript
// Get the password field and toggle checkbox
var password = document.querySelector('#password');
var toggle = document.querySelector('#show-password');

// Listen for click events on the toggle
toggle.addEventListener('click', function (event) {

 // If the toggle is checked, change the type to "text"
 // Otherwise, change it back to "password"
 if (toggle.checked) {
  password.type = 'text';
 } else {
  password.type = 'password';
 }

}, false);


````
