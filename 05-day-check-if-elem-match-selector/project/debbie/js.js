/** Element.matches() polyfill
 ** https://developer.mozilla.org/en-US/docs/Web/API/Element/matches#Polyfill
 */
if (!Element.prototype.matches) {
	Element.prototype.matches = Element.prototype.msMatchesSelector || Element.prototype.webkitMatchesSelector;
}

// Get the elements, convert nodelist to array
var pwd = document.querySelector('#password');
var pwords = Array.prototype.slice.call(document.querySelectorAll('input[type="password"]') );

// Listen for events document-wide
document.addEventListener('click', function(event) {
    // Does the clicked element have
    // the [data-pw-toggle] attribute?
    // If not, stop.
    if (!event.target.matches('[data-pw-toggle]')) return;
    
    // Get elements and convert
    // nodelist to an array
    var pwords = Array.prototype.slice.call(document.querySelectorAll(event.target.getAttribute('data-pw-toggle')));
    // Loop through password fields
    pwords.forEach(function (pw) {
        if (event.target.checked) {
           pw.type = 'text';
        } else {           
           pw.type= 'password';
        }      
    })
}, false );