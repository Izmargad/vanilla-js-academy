//revealing module pattern
//Turn this object literal into a module that only exposes the render method

// let myModule = {
//     data: [],
//     render: () => {

//     },
//     add: () => {

//     },
//     remove: () => {

//     }
// };

let myModule = (function(){

    let _data = [];

    let _render = function () {
        // click listeners for _add and _remove
    }
    let _add = function () {
        _data.push('asdf');
    }
    let remove = function () {
        _data.pop();
    }
    return {
        render: _render
    }
})();
myModule.render();
myModule._data; // will fail
myModule._add(); // will fail
