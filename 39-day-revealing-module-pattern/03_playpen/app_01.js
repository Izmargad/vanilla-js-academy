<script>

var _ = (function () {

    var buttons = document.querySelectorAll('button');

    // Holds our public methods
    var methods = {};

    
    /**
     * Convert nodeList to array
     * @param  {NodeList} nodelist  List of nodes
     * @return {Array}     			An array
     */
    methods.convertToArray = function(nodelist) {
        return Array.prototype.slice.call(nodelist);
    };


    /**
     * Copy an array
     * @param  {Array} arr The original
     * @return {Array}     A copy
     */
    var copy = function (arr) {
        return arr.slice();
    };


    /**
     * Get an immutable copy of the buttons
     * @param  {Array} The buttons
     */
    methods.getButtons = function () {
        return copy(methods.convertToArray(buttons));
    };


    /**
     * Get first element on page
     * @param  {String} selector Selector to match
     * @return {Object}     	 DOM element
     */
    methods.getFirstElement = function(selector) {
        return document.querySelector(selector);
    };


    /**
     * Get first element on page
     * @param  {String} selector Selector to match
     * @return {NodeList}     	 DOM elements
     */
    methods.getAllElements = function(selector) {
        return methods.convertToArray(document.querySelectorAll(selector));
    };


    /**
     * Add class to selected elements
     * @param  {Array} array      Array of elements
     * @param  {String} classname Class name to add
     */
    methods.addClass = function(array, classname) {
        array.forEach(function (element) {
            element.classList.add(classname);
        });
    };


    /**
     * Remove class from selected elements
     * @param  {Array} array      Array of elements
     * @param  {String} classname Class name to remove
     */
    methods.removeClass = function(array, classname) {
        array.forEach(function (element) {
            element.classList.remove(classname);
        });
    };

    // Return public methods
    return methods;

})();

console.log("return array of button elements")
console.log(_.getButtons('button'));
console.log("return first button element")
console.log(_.getFirstElement('button'));
console.log("return all buttons as allButtons variable");
var allButtons = _.getAllElements('button');
console.log(allButtons);

var buttonColours = new Promise(function (resolve, reject) {
setTimeout(function () {
    console.log("remove blue class from buttons");
    resolve(_.removeClass(allButtons, 'btn-blue'));
}, 1500);
});

buttonColours.then(function () {
setTimeout(function () {
    console.log("add purple class to buttons");
    _.addClass(allButtons, 'btn-purple');
}, 1500);
});

</script>