/**
 * Create a helper library with public methods you can use to…

    Convert a NodeList to an array.
    Get the first matching element in the DOM.
    Get all matching elements in the DOM as an array.
    Add a class to all elements in an array.
    Remove a class from all elements in an array.

For testing purposes, you can try removing the .btn-blue class and adding the .btn-purple class to your elements.
 */

// Start by creating an IIFE 
// The returned object functions gets assigned to our variable 
// handyDom is a collection of handy dom methods 
var handyDom = (function() {

	'use strict';

	// we define the relevant (button) element(s) to the DOM
	var buttons = document.querySelectorAll('button');

	// Holds our public methods
	var methods = {}

	//
	// Methods
	//
	// Helper Method 1: Convert a NodeList to an array
	// If we had a polyfill we could use the Array.from() method
	// methods.toArray = list => Array.from(list)

	// this will hold the returned node-list which will be an array
	//var nodeList = [];

	methods.listToArray = function(nodeList) {
				return Array.prototype.slice.call(nodeList);
			};

			// Create a copy of the original array
			var copy = function (arr) {
				return arr.slice();
			};

			// Now we want an Immutable copy of the buttons
	methods.getButtons = function () {
				return copy(methods.listToArray(buttons));
			};
	// Helper Method 2: Get the first matching element in the DOM
	methods.getFirstMatchingElement = function(selector) {
		return document.querySelector(selector);
	};
	
	// Helper Method 3: Get all matching elements in the DOM as an array
	methods.getAllMatchingElements = function(selector) {
	return methods.listToArray(document.querySelectorAll(selector));
	};

	// Helper Method 4: Add a class to all elements in an array
	methods.addClassToAllElements = function(array, className) {
		array.forEach(function (element) {
			element.classList.add(className);
		});
	};

	// Helper Method 5: Remove a class from all elements in an array
	methods.removeClassFromAllElements = function(array, className) {
		array.forEach(function (element) {
			element.classList.remove(className);
		});
	};
	// Izzi Customization
	
	methods.addClass = function (arr, className) {
		var addMyClass = function (element){
		  addClassToElement(element, className);
		};
		if (!Array.isArray(arr)) { addMyClass(arr); return }
		arr.map(addMyClass);
	  };

	//
	// Return the public methods
	//
	return methods;
	
})();

// handyDom.listToArray
var arr = handyDom.listToArray(document.querySelectorAll('button'))
console.log('handyDom.listToArray()', arr)

// handyDom.getFirstMatchingElement()
var button = handyDom.getFirstMatchingElement('#button-2');
console.log('handyDom.getFirstMatchingElement()', button);

// handyDom.getAll()
var buttons = handyDom.getAllMatchingElements('button');
console.log('handyDom.getAllMatchingElements()', buttons);

// handyDom.addClassToAllElements()
handyDom.addClassToAllElements(buttons, 'btn-purple');

// handyDom.removeClassFromAllElements()
handyDom.removeClassFromAllElements(buttons, 'btn-blue');

// handyDom.listToArray
var arr = handyDom.listToArray(document.querySelectorAll('button'))
console.log('UPDATED Colour: handyDom.listToArray()', arr)
