;(function () {

  // Opt into ES5 strict mode
  "use strict";

  //
  // Variables
  //

  // Store the required endpoints
  var endpoints = {
    location: "https://ipapi.co/json",
    weather: {
      url: "https://api.weatherbit.io/v2.0/current",
      apiKey: "6030c62e6d3448ffa355b66dda6b507c"
    }
  };

  // Get the #app element
  var app = document.querySelector("#app");


  //
  // Functions
  //

  /**
   * Sanitize and encode all HTML in a user-submitted string
   * (c) 2018 Chris Ferdinandi, MIT License, https://gomakethings.com
   * @param   {String} str  The user-submitted string
   * @returns {String}      The sanitized string
   */
  function sanitizeHTML (str) {

    var temp = document.createElement("div");
    temp.textContent = str;
    return temp.innerHTML;

  }

  /**
   * Get the JSON data from a Fetch request
   * @param   {Object} response The response to the Fetch request
   * @returns {Object}          The JSON data OR a rejected promise
   */
  function getJSON (response) {
    return response.ok ? response.json() : Promise.reject(response);
  }

  /**
   * Get the user's coordinates
   * @param   {Object} data The data returned by the IP API
   * @returns {Object}      The user's coordinates
   */
  function getCoords (data) {

    return {
      lat: sanitizeHTML(data.latitude),
      lon: sanitizeHTML(data.longitude)
    };

  }

  /**
   * Get the user's location
   * @returns {Object} The user's location
   */
  function getLocation () {
    return fetch(endpoints.location).then(getJSON).then(getCoords);
  }

  /**
   * Get the full endpoint for the Weatherbit API
   * @param   {Object} location  The location returned by the IP API
   * @returns {String}           The endpoint
   */
  function getEndpoint (location) {
    return endpoints.weather.url + "?key=" + endpoints.weather.apiKey + "&lat=" + location.lat + "&lon=" + location.lon;
  }

  /**
   * Get the required data from the Weatherbit API
   * @param   {Object} data  The data returned by the API
   * @returns {Object}       The required data
   */
  function getData (data) {

    // Get the actual data object
    data = data.data[0];

    // Return only the required data
    console.log(data.app_temp)
    return {
      app_temp: sanitizeHTML(data.app_temp),
      
      city_name: sanitizeHTML(data.city_name),
      last_ob_time: sanitizeHTML(data.last_ob_time),
      weather: {
        description: sanitizeHTML(data.weather.description),
        icon: sanitizeHTML(data.weather.icon)
        
      }
      
    };

  }

  /**
   * Get the user's local weather info
   * @param   {Object} location The location returned by the IP API
   * @returns {Object}          The user's local weather info
   */
  function getWeather (location) {
    return fetch(getEndpoint(location)).then(getJSON).then(getData);
  }

  /**
   * Show the user's local weather info
   * @param {Object} data The info returned by the Weatherbit API
   */
  function showWeather (data) {

    app.innerHTML = (
      "<h2>" + data.city_name + "</h2>" +
      "<div class='weather-temp'>" +
        "<img src='https://www.weatherbit.io/static/img/icons/" + data.weather.icon + ".png' alt='" + data.weather.description + "'>" +
        "<p><b>" + data.app_temp + "<sup>&deg;C</sup></b></p>" +
      "</div>" +
      "<p class='weather-desc'>" + data.weather.description + "</p>" +
      "<p>" +
        "<strong>Last observed: </strong>" +
        "<time datetime='" + data.last_ob_time + "'>" + new Date(data.last_ob_time).toLocaleTimeString() + "</time>" +
      "</p>"
    );

  }


  //
  // Inits & Event Listeners
  //

  // Show the user's local weather info
  getLocation().then(getWeather).then(showWeather);

})();