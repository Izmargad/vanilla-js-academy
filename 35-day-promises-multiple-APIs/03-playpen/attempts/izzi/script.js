/**
 * Project: Weather App
 * Gets the user's location
 * Display the current weather information
 */

/**
 * Services used in this application
 * https://www.weatherbit.io/
 * https://ipapi.co/
 */

;(function () {

  // Opt into ES5 strict mode
  "use strict";

  /**
   * Variables: The Data we work with
   * 
   * The application needs to know:
   * The current user location
   * The weather based on the user location
   * 
   * ***** Data Endpoints from the Services ***** 
   * 
   * The data end-points for fetching the weather
   * The data end-point to return the user's location 
   * Store the API Key
   */
  //
  // Variables
  //

  var api = '6030c62e6d3448ffa355b66dda6b507c';
  var keyString = 'current?key='; // to make it cleaner later
  var locationEndPoint = 'https://ipapi.co/json';
  var weatherEndPoint = 'https://api.weatherbit.io/v2.0/current?key=';
var icons = 'https://www.weatherbit.io/static/img/icons/';
  // **** User Details **** //
  var location = locationEndPoint;
  var app = document.querySelector('#app');



  /**
   * Functions: 
   * The Actions we need to handle
   * The Calculations we need to perform
   *  
   * The application needs to know:
   * The current user location
   * The weather based on the user location
   * 
   * ***** Data Endpoints from the Services ***** 
   * 
   * The data end-points for fetching the weather
   * The data end-point to return the user's location 
   * Store the API Key
   */

  // Put your functions here
  // make a call to the location service
  // return the current location
  // render the location to the DOM

  //  function getJSON(response) {
  //    return response.ok ? response.json() : Promise.reject(response);
  //  }

  // Call the API
  function getWeather(){
  fetch(locationEndPoint).then(function (response) {
    if (response.ok) {
      return response.json();
    } else {
      return Promise.reject(response);
    }
  }).then(function (data) {

    // Store the fetched location data to a variable
    location = data;

    // Fetch another API (the Weather API this time)
    console.log(location);
    return fetch(weatherEndPoint + api + '&lat=' + data.latitude +
    '&lon=' + data.longitude);

  }).then( function ( response ) {
		if ( response.ok ) {
			return response.json();
		} else {
			return Promise.reject( response );
		}
	}).then( function ( weatherData ) {
		console.log( location, weatherData, weatherData.data[0].app_temp );
		app.innerHTML = `<h2>${ location.city }, ${ location.region }</h2>
		<figure>
			<img alt="${ weatherData.data[0].weather.description }" height="100" width="100" src="${icons}${ weatherData.data[0].weather.icon }.png">
		</figure>
		<p>${ weatherData.data[0].app_temp }&deg;C</p>`;
	}).catch( function ( error ) {
		console.warn( error );
  });
}
  //
  // Inits & Event Listeners
  //

  // Initialize your script here and add any event listeners
// Get the location on page load


// Display the weather information if the page is fully loaded.
getWeather();

})();