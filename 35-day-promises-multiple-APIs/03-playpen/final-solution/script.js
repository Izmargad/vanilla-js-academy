 /**
  * 1. [ ] is 'n set
  * 2. + is one or more
  * 3. * is zero or more
  * 4. ^ match begin
  * 5. $ match einde
  */
 
 // Get the table of contents container
 var tocSection = document.querySelector('#table-of-contents');

  // I identify the Table of Contents section for the DOM Robot 
  // This was a data attribute previously made it simpler 
 var toc = document.querySelectorAll('h2');

 // Make sure there's at least one heading
 // Only generate markup if there are heading elements
 // Added after review from Chris Ferdinandi's code
 function display(){
 if (toc.length > 0) {

   // Set the HTML for the table of contents container
   // Add a heading and an ordered list
   // Convert the headings NodeList to an array, and use the array.map() method
   // to create an array of markup strings that we'll combine with the array.join() method
   tocSection.innerHTML =
     '<h2>Table of Contents</h2>' +
     '<ul>' +
     // now I want to id the h2 elements as an array
     // only h2 items will be listed 
       Array.prototype.slice.call(toc).map(function (heading, index) {
        // I am checking if I have the toc now at this stage  
        toc.forEach(function (toc, index) {
       // console.log(toc.textContent);
    });
// check if the h2 heading has an id
// assign the variable id to it
         if (heading.id.length > 0) {
           var id = heading.id;
         } else {
           // if there is no id on the h2, take the textContent inside the h2
           // replace the id with it and use the regex for fancy formatting
           heading.id = heading.textContent.replace(new RegExp(/[^a-z0-9]+/,'gi'), '-');
         }
         // render and return
         return '<li><a href="#' + heading.id + '">' + heading.textContent + '</a></li>';
       }).join('') +
     '</ul>';
 }
}
display();
console.log(tocSection);
console.log(toc);