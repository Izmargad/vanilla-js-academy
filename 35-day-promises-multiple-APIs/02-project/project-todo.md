# Project: Table of Contents - Missing IDs

In today’s project, we’re going to modify our Table of Contents script to support headings that don’t have IDs on them.

## The Template

We’ll use yesterday’s solution as the starting point for today’s project.

The markup has been modified so that certain headings don’t have IDs on them. When creating your table of contents, if the heading doesn’t have an ID to link to, create one and assign it to the heading.

````html

<div id="table-of-contents"></div>

<h2>Cat O'Nine Tails</h2>
<p>Cat o'nine tails Pieces of Eight swab carouser tackle. Pink hornswaggle gabion Sea Legs Davy Jones' Locker.</p>
<p>Hang the jib Nelsons folly trysail ahoy prow. Transom strike colors scallywag aft league.</p>

````
