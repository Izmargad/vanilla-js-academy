// Lessons from Vanilla JS Academy
// This code originally by Chris Ferdinandi
// As seen:
// https://gomakethings.com/using-stateful-components-to-build-a-stopwatch-web-app-with-vanilla-js/


//
// Variables
//

var stopwatch, timer;
var duration = 120;


//
// Methods
//

/**
 * A vanilla JS helper for creating state-based components
 * @param {String|Node} elem    The element to make into a component
 * @param {Object}      options The component options
 */
var Component = (function () {

	'use strict';

	/**
	 * Create the Component object
	 * @param {String|Node} elem    The element to make into a component
	 * @param {Object}      options The component options
	 */
	var Component = function (elem, options) {
		if (!elem) throw 'ComponentJS: You did not provide an element to make into a component.';
		this.elem = elem;
		this.data = options ? options.data : null;
		this.template = options ? options.template : null;
	};

	/**
	 * Sanitize and encode all HTML in a user-submitted string
	 * @param  {String} str  The user-submitted string
	 * @return {String}      The sanitized string
	 */
	Component.sanitize = function (str) {
		var temp = document.createElement('div');
		temp.textContent = str;
		return temp.innerHTML;
	};

	/**
	 * Render a template into the DOM
	 * @return {[type]}                   The element
	 */
	Component.prototype.render = function () {

		// Make sure there's a template
		if (!this.template) throw 'ComponentJS: No template was provided.';

		// If elem is an element, use it.
		// If it's a selector, get it.
		var elem = typeof this.elem === 'string' ? document.querySelector(this.elem) : this.elem;
		if (!elem) return;

		// Get the template
		var template = (typeof this.template === 'function' ? this.template(this.data) : this.template);
		if (['string', 'number'].indexOf(typeof template) === -1) return;

		// Render the template into the element
		if (elem.innerHTML === template) return;
		elem.innerHTML = template;

		// Dispatch a render event
		if (typeof window.CustomEvent === 'function') {
			var event = new CustomEvent('render', {
				bubbles: true
			});
			elem.dispatchEvent(event);
		}

		// Return the elem for use elsewhere
		return elem;

	};

	return Component;

})();

/**
 * Format the time in seconds into hours, minutes, and seconds
 * @param  {Number} time The time in seconds
 * @return {String}      The time in hours, minutes, and seconds
 */
var formatTime = function (time) {
	var minutes = parseInt(time / 60, 10);
	var hours = parseInt(minutes / 60, 10);
	if (minutes > 59) {
		minutes = minutes % 60;
	}
	return (hours > 0 ? hours + 'h ' : '') + (minutes > 0 || hours > 0 ? minutes + 'm ' : '') + (time % 60) + 's';
};

/**
 * Setup the stopwatch on page load
 */
var setup = function () {

	// Create the stopwatch
	stopwatch = new Component('#app', {
		data: {
			time: duration,
			running: false
		},
		template: function (props) {
			var template =
				'<div id="stopwatch">' +
					formatTime(props.time) +
				'</div>' +
				'<p>' +
					'<button data-stopwatch="' + (props.running ? 'stop' : 'start') + '">' + (props.running ? 'Stop' : 'Start') + '</button>' +
					'<button data-stopwatch="reset">Reset</button>' +
				'</p>';
			return template;
		}
	});

	// Render the stopwatch into the DOM
	stopwatch.render();

};

/**
 * Start the stopwatch
 */
var start = function () {

	// Render immediately
	stopwatch.data.running = true;
	stopwatch.render();

	// Start the timer
	timer = window.setInterval(function () {

		// Update the timer
		stopwatch.data.time = stopwatch.data.time - 1;
		stopwatch.render();

	}, 1000);

};

/**
 * Stop the stopwatch
 */
var stop = function () {
	stopwatch.data.running = false;
	window.clearInterval(timer);
	stopwatch.render();
};

/**
 * Reset the stopwatch
 */
var reset = function () {
	stopwatch.data.time = duration;
	stop();
};

/**
 * Handle click events
 */
var clickHandler = function (event) {

	// Check if a stopwatch action button was clicked
	var action = event.target.getAttribute('data-stopwatch');
	if (!action) return;

	// If it's the start button, start
	if (action === 'start') {
		start();
		return;
	}

	// If it's the stop button, stop
	if (action === 'stop') {
		stop();
		return;
	}

	// If it's the stopwatch button, reset
	if (action === 'reset') {
		reset();
	}

};


//
// Inits & Event Listeners
//

// Setup the app
setup();

// Listen for clicks
document.addEventListener('click', clickHandler, false);