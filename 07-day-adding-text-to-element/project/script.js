        // Practice using data attributes ('[data-input-text');
        var elem = document.querySelector('[data-input-text]');
        var countTypedContent = document.querySelector('[data-char-display]');

        function countCharacters() {

            countTypedContent.textContent = elem.value.length;
            //console.log(countTypedContent.textContent);
        }
        window.addEventListener('DOMContentLoaded', countCharacters, false);
        window.addEventListener('input',countCharacters, false);
        //elem.addEventListener('keyup', countCharacters);
        //elem.addEventListener('input', countCharacters);