;(function() {
    'use strict'
    const list = new _DML('li');
    const buttons = new _DML('button');
    const numList = new _DML('ol', 'li');
    const indigoButton = new _DML('#button-7');

    console.log('The buttons I could find are: ', buttons);
    console.log('The unordered list items I could find are: ', list);
    console.log('The numbered items are: ', numList);

    // Get the first matching item from the DOM
    const firstListItem = list.getFirst();
    console.log('The first list item is: ', firstListItem.textContent);

    // Get the last matching item from the DOM
    const lastListItem = list.getLast();
    console.log('I am the last item listed: ', lastListItem.textContent);

    // Remove a class from all matching elements
    buttons.removeClass('btn-blue');

    // Add a class from all matching elements
    //buttons.addClass('btn-purple');
    //indigoButton.removeClass('btn-purple')
    //indigoButton.addClass('btn-pink');

    // Remove a class from, then add a class to, all matching elements
	buttons.removeClass('btn-blue').addClass('btn-pink')

       
    })();