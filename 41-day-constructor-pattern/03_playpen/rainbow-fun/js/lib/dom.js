        /**
 * Create a small DOM manipulation library with public methods you can use to…

    Get an array of items from the DOM.
    Get the first and last matching items from the DOM.
    Add and remove a class to all matching elements.
    
You should be able to pass in a selector, 
call a helper function
on your matching elements
 */

        // Start by creating an IIFE 
        // The returned object functions gets assigned to our variable 
        // DML is a DOM Manipulation Library
        // It consists of a collection of handy dom methods 
        var _DML = (function () {

            /**
             * The constructor object
             */
            const Constructor = function (selector) {
                this.elements = Array.prototype.slice.call(document.querySelectorAll(selector, 0));
            }


            // Now we want an Immutable copy of the elements
            // Thanks for reminding me of this @Kieran Barker
            Constructor.prototype.items = function () {
                return Array.prototype.slice.call(this.elements);
            };

            // Construct an object prototype
            // This object prototype gets the first item in a set of elements
            Constructor.prototype.getFirst = function () {
                return this.elements[0]
                
            }
            // Returns first element that matches selector given when initializing the library
            // Constructor.prototype.getFirst = function () {
            //     return document.querySelector(this.selector);
            // }
            // Construct an object prototype
            // This object prototype gets the last item in a set of elements
            Constructor.prototype.getLast = function () {
                return this.elements[this.elements.length - 1]
            }

            // Construct an object prototype
            // This object prototype adds a class to every item in a set of elements
            Constructor.prototype.addClass = function (className) {
                this.elements.forEach(el => el.classList.add(className))
                return this

            }
            // Construct an object prototype
            // This object prototype adds a class to only a single element
            Constructor.prototype.addClassToSingleElement = function (className) {
                this.elements.classList.add(className)
                return this
            }
            // Construct an object prototype
            // This object prototype removes a class from every item in a set of elements
            Constructor.prototype.removeClass = function (className) {
                this.elements.forEach(el => el.classList.remove(className))
                return this
            }

            return Constructor;

        })();
