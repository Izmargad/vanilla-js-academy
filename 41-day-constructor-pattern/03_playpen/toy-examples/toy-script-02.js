class Fruit {
    constructor(name, taste) {
      this.name = name;
      this.taste = taste;
    }
  }
  
  Fruit.prototype.getTaste = function () {
    return `The ${this.name} tastes ${this.taste}.`;
  }
  
  const apple = new Fruit('apple', 'sweet');
  const orange = new Fruit('orange', 'tangy');
  
  console.log(apple.getTaste()); // The apple tastes sweet.
  console.log(orange.getTaste()); // The orange tastes tangy.