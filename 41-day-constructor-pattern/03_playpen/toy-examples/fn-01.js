/**
 * Basic function structure
 * Example of a function, add, which adds to values 
 * It returns the result of the sum of x and y
 * @param {First numeric to use in addition} x 
 * @param {Second numeric value to use in addition} y 
 */

function add (x,y){
    return x + y;
    
  }

// another function that can take in values as well
function addFive(x, addReference){
return addReference(x, 5)
}

// I add a function, into another function
// I added add to addFive

addFive(10, add) // returns 15 

  // I press the machine and enter values, say 3 and 5
//   const me = add;
//   myAnswer = me(3,5);
//   console.log('My answer is ', myAnswer)

  // You press the machine and enter values. say 4 and 5
//   const you = add;
//   yourAnswer = you(4,5)
//     console.log('Your answer is ', yourAnswer)
  