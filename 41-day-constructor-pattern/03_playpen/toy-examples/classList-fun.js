/* 
|>.querySelectorAll()
|| Collect all .cell into a "static" NodeList

|>Array.from()
|| Convert NodeList into an array

|~.getElementsByClassName() 
|| Does the same as .querySelectorAll({.class})
|| except that gEBCN() returns a "Live Collection"
|| which means that it changes in real time so
|| that there's a good chance you may end up with
|| half the expected length of the collection.
|| 99% of the time .qSA() is the better choice
|| because it returns a "static" NodeList.
*/
var cellArray = Array.prototype.slice.call(document.querySelectorAll('.cell'));

/*
|>.forEach()
|| Iterate through array and invoke a function on 
|| each node.
*/


cellArray.forEach(function(node, idx) {
    console.log(node);

  /*
  |>.classList.add()
  || Adds a class to a node's list of classes
  */
  node.classList.add('transiting');
});