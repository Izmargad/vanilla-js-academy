;(function () {

  'use strict';

  //
  // Variables
  //

  // Variables for the timer and timer ID
  let timer, timerID;


  //
  // Functions
  //

  /**
   * Create a handler object for new Proxy objects
   * @param   {Object} instance The current instance of Timer
   * @returns {Object}          The handler object
   */
  function handler (instance) {

    return {

      // If the property is an object or array, recursively return a new Proxy.
      // Otherwise, just return the property like normal.
      get: function (obj, prop) {
        if (['[object Object]', '[object Array]'].indexOf(Object.prototype.toString.call(obj[prop])) > -1) {
          return new Proxy(obj[prop], handler(instance));
        }
        return obj[prop];
      },

      // Re-render the instance when setting a property
      set: function (obj, prop, value) {
        obj[prop] = value;
        instance.render();
        return true;
      },

      // // Re-render the instance when deleting a property
      deleteProperty: function (obj, prop) {
        delete obj[prop];
        instance.render();
        return true;
      }

    };

  }

  /**
   * Create a new timer
   * @param {String} selector A valid CSS selector
   * @param {Object} options  Options for the timer
   */
  function Timer (selector, options) {

    // Public element and template properties
    this.element = document.querySelector(selector);
    this.template = options.template;

    // Private _data property
    let _data = new Proxy(options.data, handler(this));

    // Getter and setter for public data property.
    // I'm using arrow functions to make sure `this` points to Timer.
    Object.defineProperty(this, 'data', {

      get: () => {
        return _data;
      },

      set: (data) => {
        _data = new Proxy(data, handler(this));
        this.render();
        return true;
      }

    });

  }

  /**
   * Render the timer
   */
  Timer.prototype.render = function () {
    this.element.innerHTML = this.template(this.data);
  };

  /**
   * Get the time remaining
   * @param   {Object} props The component data
   * @returns {String}       The formatted time
   */
  function getTime (props) {

    // Calculate the time remaining
    let time = {
      minutes: Math.floor(props.time / 60).toString(),
      seconds: (props.time % 60).toString()
    };

    // Return this as a formatted string (M:SS)
    return time.minutes + ':' + time.seconds.padStart(2, '0');

  }

  /**
   * Get the controls for the timer
   * @param   {Object} props The component data
   * @returns {String}       An HTML string
   */
  function getControls (props) {

    // Get the action of the start/stop button
    let action = props.paused ? 'Start' : 'Stop';

    // Return the buttons as a string
    return `
      <button id="start-stop" type="button">${action}</button>
      <button id="reset" type="button">Reset</button>
    `;

  }

  /**
   * Template for how the UI should look
   * @param   {Object} props The component data
   * @returns {String}       The template
   */
  function template (props) {

    // If the timer is done, show a reset button
    if (props.done) {
      return `
        <button id="reset" type="button">Reset</button>
      `;
    }

    // Otherwise, show the time remaining
    return `
      <p class="time">
        ${getTime(props)}
      </p>
      <div class="controls">
        ${getControls(props)}
      </div>
    `;

  }

  /**
   * Get the data for the initial UI
   * @returns {Object} The data
   */
  function getStartingData () {

    return {
      time: 120,
      paused: true,
      done: false
    };

  }

  /**
   * Countdown function to be invoked once every second
   */
  function countDown () {

    // If there's time remaining, decrease it
    if (timer.data.time > 0) {
      timer.data.time--;
      return;
    }

    // Otherwise, stop the timer
    timer.data.done = true;
    clearInterval(timerID);

  }

  /**
   * Handle the start/stop button
   * @param {Object} event The Event object
   */
  function startStop(event) {

    // Start the countdown
    if (event.target.textContent === "Start") {
      timerID = setInterval(countDown, 1000);
    }

    // Stop the countdown
    else if (event.target.textContent === "Stop") {
      clearInterval(timerID);
    }

    // Changed paused state
    timer.data.paused = !timer.data.paused;

  }

  /**
   * Reset the timer
   * @param {Object} event The Event object
   */
  function reset (event) {

    // Stop the timer
    clearInterval(timerID);

    // Reset the data
    timer.data = getStartingData();

  }

  /**
   * Handle click events
   * @param {Object} event The Event object
   */
  function handleClick (event) {

    // Handle start/stop
    if (event.target.matches('#start-stop')) {
      startStop(event);
      return;
    }

    // Handle reset
    if (event.target.matches('#reset')) {
      reset(event);
      return;
    }

  }


  //
  // Inits & Event Listeners
  //

  // Create a new timer
  timer = new Timer('#app', {
    template: template,
    data: getStartingData()
  });

  // Run an initial render
  timer.render();

  // Handle click events
  timer.element.addEventListener('click', handleClick);

})();