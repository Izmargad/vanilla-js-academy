# Timer - Reactive Data

A Pen created on CodePen.io. Original URL: [https://codepen.io/kieranbarker/pen/PoZBYPW](https://codepen.io/kieranbarker/pen/PoZBYPW).

**A state-based timer that uses reactive data.**

Instead of explicitly calling the `render()` method every time I update the `data` object, I set up the component to use Proxies and reactive data. This means it **automatically** re-renders the UI whenever I update the data.

_Want to learn how to build this? Join the next session of the [Vanilla JS Academy](https://vanillajsacademy.com/)._
