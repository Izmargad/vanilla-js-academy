# Academy Foundations

This is my template for [Vanilla JS Academy](https://vanillajsacademy.com/) projects 🍦

It comes preloaded with:

* A heading and paragraph
* Barebones styles for centering the page
* The default bundle from [polyfill.io](https://polyfill.io/v3/)
* An [immediately-invoked function expression (IIFE)](https://gomakethings.com/the-anatomy-of-an-immediately-invoked-function-expression/)
* A `.gitignore` for Node.js

Feel free to use it as a boilerplate. You can remove anything you don't need 🙂

[**Use This Template &rarr;**](https://github.com/kieranbarker/academy-foundations/generate)
