
/**
 * Automatically save the form field data as the user types
 * As the user types, the value of a form field should automatically be saved. 
 * When the page is reloaded or opened back up, the form should be repopulated 
 * with the user’s saved data. 
 * If the form is submitted, clear the data from storage and wipe the form.
 */
;(function() {
	'use strict';

	//
	// Variables
	//
	// The DOM needs to be aware of the Form with which we interact
	var form = document.querySelector('#save-me');

	//
	// Methods
	//

	// We need to set the data, by handling the input => localStorage.setItem()
	// We need to restore / get the data previously entered => localStorage.getItem()
	// We need to remove the data on submission => localStorage.removeItem()

	/**
	 * Handle the input events 
	 * @param {*} evt 
	 */
	// I am adding everything entered / captured via the form into the storage object
	var inputHandler = function (evt) {
		if (!event.target.closest('#save-me')) 
		return;
		localStorage.setItem(evt.target.name, evt.target.value);
	}


	// var submitHandler = function (evt) {
	// 	if (!evt.target.closest('form').matches('#save-me')) 
	// 	return;
	// 	// remove all the entered values from the storage object
	// 	localStorage.clear();
		
	// **** NOT A GOOD IDEA TO USE localStorage.clear() ****
	// **** It will clear everything in the domain ****	
	// };
	
	function submitHandler(){
		for (var item in localStorage) {
			if (localStorage.hasOwnProperty(item)) {
				form[item].value = localStorage.removeItem(item);
			}
		}
	}
/**
 * Render the previously stored items to the form
 */
 function loadFromStorage(){
		for (var item in localStorage) {
			if (localStorage.hasOwnProperty(item)) {
				form[item].value = localStorage.getItem(item);
			}
		}
	}

	//
	// Inits & Event Listeners
	//

	// Load saved data from storage
	loadFromStorage();
	//populateForm();

	// Listen for input events
	document.addEventListener('input', inputHandler, false);

	// Listen for submit events
	document.addEventListener('submit', submitHandler, false);
})();
