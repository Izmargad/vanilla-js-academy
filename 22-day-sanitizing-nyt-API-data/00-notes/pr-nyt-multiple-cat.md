# Project Review: NYT Multiple Categories

The first, sections, is an array of the NYT sections I’d like to include in my app. The second, numberOfArticles, is how many articles I’d like to include for each section.

Setting them right at the start of the app makes it easier to update later if I wanted to change things up.

````javascript
// Get the app
var app = document.querySelector('#app');

// Store API key to a variable for easier access
var apiKey = '1234abcd';

// Set the sections and number of articles to show to variables so they can be easily changed later
var sections = ['technology', 'science', 'magazine'];
var numberOfArticles = 3;
````

Next, I moved my fetch() call into a helper function: getArticles().

This function accepts the section name as an argument. In my API endpoint, I used the section variable instead of the hard-coded home endpoint I had before.

````javascript
/**
 * Get articles for a section
 * @param  {String} section The section name
 */
var getArticles = function (section) {
 fetch('https://api.nytimes.com/svc/topstories/v2/' + section + '.json?api-key=' + apiKey).then(function (response) {
  if (response.ok) {
   return response.json();
  } else {
   return Promise.reject(response);
  }
 }).then(function (data) {

  // Render them into the DOM
  render(data.results);

 }).catch(function (error) {
  console.log('Something went wrong:', error);
 });
};
````

Because the sections I want to include are in an array, I can use the Array.forEach() method to loop through them and pass them into the getArticles() function.

````javascript
// Get articles for each section
sections.forEach(function (section) {
 getArticles(section);
});
````

Next, I created a helper function, getFirstFew(), to get back a subset of all of the articles.

It uses the Array.slice() method to get the first few articles from the array, using numberOfArticles as the amount of articles to keep. Then it returns the result.

````javascript
/**

* Get the first few articles from the full data set
* @param  {Array} articles The full set of articles
* @return {Array}          The first few
 */
var getFirstFew = function (articles) {
 return articles.slice(0, numberOfArticles);
};
````

In my getArticles() function, I pass the data.results into getFirstFew(). Then, I pass it and the section name into my render() function.

````javascript
/**

* Get articles for a section
* @param  {String} section The section name
 */
var getArticles = function (section) {
 fetch('https://api.nytimes.com/svc/topstories/v2/' + section + '.json?api-key=' + apiKey).then(function (response) {
  if (response.ok) {
   return response.json();
  } else {
   return Promise.reject(response);
  }
 }).then(function (data) {

  // Get the first few articles
  var firstFew = getFirstFew(data.results);

  // Render them into the DOM
  render(firstFew, section);

 }).catch(function (error) {
  console.log('Something went wrong:', error);
 });
};
````

In my render() function, I’ve added a new argument: section.

Instead of setting the innerHTML of my app element, I use += to append my HTML string into after whatever is already there. Before the articles.map() function, I add an h2 heading with the section name.

I’m ok with updating the UI several times here because we’re only doing it for the major sections, and not for each article. I also don’t want UI rendering to be delayed if one section fails or takes longer to return.

````javascript
/**

* Render articles into the DOM
* @param  {Array} articles The articles to render
 */
var render = function (articles, section) {

 // Create a new array of markup strings with array.map(), then
 // Combine them into one string with array.join(), then
 // Insert them into the DOM with innerHTML
 app.innerHTML += '<h2>' + section + '</h2>' + articles.map(function (article) {
  var html =
   '<article>' +
    '<h3><a href="' + article.url + '">' + article.title + '</a></h3>' +
    '<p>' + article.byline + '</p>' +
    '<p>' + article.abstract + '</p>' +
   '</article>';
  return html;
 }).join('');

};
````

Now, I can render multiple sections into my app.

Using the variables at the top of my script, I can easily change the sections and number of articles to use.
