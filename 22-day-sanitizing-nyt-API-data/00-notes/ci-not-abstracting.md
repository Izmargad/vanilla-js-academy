# Common Issue: Not Abstracting

Something that takes time to really feel second-nature is abstracting your code. With this project, something I often see is repeating API calls manually for each function.

Sometimes, that means manually calling the getArticles() function for each section.

````javascript
// Get articles for each section
getArticles('technology');
getArticles('science');
getArticles('magazine');

````

This is a little less than ideal, but not terrible. If you want to include a lot of sections it becomes harder to manage, but you’ve still got some good abstraction in other places.

Other times, I see people manually call the fetch() method itself once for each section.

````javascript
// Technology
fetch('https://api.nytimes.com/svc/topstories/v2/technology.json?api-key=' + apiKey).then(function (response) {
 if (response.ok) {
  return response.json();
 } else {
  return Promise.reject(response);
 }
}).then(function (data) {

 // Get the first few articles
 var firstFew = getFirstFew(data.results);

 // Render them into the DOM
 render(firstFew, 'technology');

}).catch(function (error) {
 console.log('Something went wrong:', error);
});

// Science
fetch('https://api.nytimes.com/svc/topstories/v2/science.json?api-key=' + apiKey).then(function (response) {
 if (response.ok) {
  return response.json();
 } else {
  return Promise.reject(response);
 }
}).then(function (data) {

 // Get the first few articles
 var firstFew = getFirstFew(data.results);

 // Render them into the DOM
 render(firstFew, 'science');

}).catch(function (error) {
 console.log('Something went wrong:', error);
});

// ...
````

This approach results in a lot of duplicate code, and should be refactored.
