
// Leading semicolon IIFE 

;(function () {
// All code inside the IIFE is run inside
// No need to name and call a function
// Get the table of contents container
var tableOfContents = document.querySelector('#table-of-contents');

// Get all of the h2 headings in the document
var headings = document.querySelectorAll('h2');

// Make sure there's at least one heading
// Only generate markup if there are heading elements
if (headings.length > 0) {

  // Set the HTML for the table of contents container
  // Add a heading and an ordered list
  // Convert the headings NodeList to an array, and use the array.map() method
  // to create an array of markup strings that we'll combine with the array.join() method
  tableOfContents.innerHTML =
    '<h2>Table of Contents</h2>' +
    '<ol>' +
      Array.prototype.slice.call(headings).map(function (heading) {

        // If the heading.id.length is less than 1, there's no ID on the element
        // We need to add one
        if (heading.id.length < 1) {
          // Replace anything that's not a letter or number with dashes
          heading.id = heading.textContent.replace(/[^a-z0-9]+/gi, '-');
        }

        return '<li><a href="#' + heading.id + '">' + heading.textContent + '</a></li>';

      }).join('') +
    '</ol>';
    }
  })();