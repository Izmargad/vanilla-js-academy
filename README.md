# Vanilla JS Code Academy

Vanilla JS Code Academy Coursework and Notes.

This repo, is based on the fabulous coursework presented by Chris Ferdinandi at the Vanilla JS Academy.

I would encourage anyone who stumbles on this, and is interested in deepening their understanding of JS and *really* learning it well, to join his academy.

## Links

- [Go Make Things](https://gomakethings.com/)
- [Vanilla JS Academy: Go Make Things Courses](https://courses.gomakethings.com/)

## Other Repos

- [Aaron Thompson](https://github.com/thompson-ad)
- [Giando](https://github.com/GiandomenicoRiceputi)
- [Giamma](https://github.com/GiammaCarioca/vanilla-js-academy)
