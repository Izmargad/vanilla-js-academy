# Day 65 - Project: Places

In today’s project, we’re going to build an app that lets you find interesting things to do near where you live.

I have a simple API setup with some data from the quirkiest state in the United States: Rhode Island.

````text
https://vanillajsacademy.com/api/places.json

````

If you’d prefer, you can create a JSON file with places near you and use that instead (you can use fetch() to get local files, too).

## The Template

The template for this project includes a heading, intro paragraph, and an empty *#app* element to render your content into.

````html
<h1>Explore</h1>
<p>Explore cool, quirky places in your own backyard.</p>

<div id="app"></div>
````

Get location data from the API, and display the places from the API in the *#app* element using state-based UI.

For this project, I would encourage you to use [Reef](https://reefjs.com/), my state-based UI library that uses the same conventions as the *Rue()* component we’ve been using so far.

````javascript
var app = new Reef('#app', {
 data: {},
 template: function (props) {
  return 'Hi there!';
 }
});

app.render();

````

It includes built-in data sanitization, and some additional extras we’ll be using in the next few lessons. I’ve preloaded it into the template for you.

````javascript
<script src="https://cdn.jsdelivr.net/npm/reefjs@7/dist/reef.js"></script>
<script>
 // Your code goes here...
</script>

````

*(You can always convert your project over later, though, if you’d prefer to stick with Rue() for now.)*
