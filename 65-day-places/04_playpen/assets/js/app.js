        
        // Create a function to fetch the data from the API
        
        //
        // Variables
        //

        
        //
        // Methods
        //
        /**
         * Step 2
		 * Get place data from the API and update the app state
		 */
		var getPlaces = function () {
			fetch('https://vanillajsacademy.com/api/places.json').then(function (response) {
				if (response.ok) {
					return response.json();
				}
                return Promise.reject(response);
                // assign the returned data to the app.data
                // under the places property
			}).then(function (data) {
				app.data.places = data;
			}).catch(function (err) {
                console.warn(err);
                // if there is an error, set the property to null
				app.data.places = null;
			});
		};

       		/**
		 * Create HTML for each of the places from the app data
		 * @param  {Object} props The app data
		 * @return {String}       The HTML
		 */
		var getPlacesHTML = function (props) {
			return props.places.map(function (place) {
				var html =
					'<div class="place">' +
						'<div>' +
							'<img alt="" src="' + place.img + '">' +
						'</div>' +
						'<div>' +
							'<h2>' + place.place + '</h2>' +
							'<p>' + place.description + '</p>' +
							'<p><em>' + place.location + '</em><br><a href="' + place.url + '">' + place.url + '</a></p>' +
						'</div>' +
					'</div>';
				return html;
			}).join('');
		};

		/**
		 * Get the message to display if there's no place data
		 * @return {String} The HTML
		 */
		var getNoPlacesHTML = function () {
			return '<p><em>Unable to find any places right now. Please try again later. Sorry!</em></p>';
		};

		/**
		 * The app component
		 */
		var app = new Reef('#app', {
			data: {},
			template: function (props) {

				// If there are places, render them
				if (props.places && props.places.length) {
					return getPlacesHTML(props);
				}

				// Otherwise, show an error
				return getNoPlacesHTML();

			}
		});


		//
		// Inits
		//

		getPlaces();