;(function () {

  // Opt into ES5 strict mode
  "use strict";

  //
  // Variables
  //

  // Component for the app
  var app = new Reef("#app", {
    data: {},
    template: template
  });

  // Endpoint for the data
  var placesAPI = "https://vanillajsacademy.com/api/places.json";


  //
  // Functions
  //

  /**
   * Get the HTML for a single listing
   * @param   {Object} place The listing data
   * @returns {String}       An HTML string
   */
  function createListing (place) {

    return (
      "<li class='listing'>" +
        "<h2>" +
          "<a href='" + place.url + "'>" + place.place + "</a>" +
        "</h2>" +
        "<img src='" + place.img + "' alt='' width='331' height='198'>" +
        "<p>" + place.description + "</p>" +
      "</li>"
    );

  }

  /**
   * Get the template for the UI
   * @param   {Object} props The component data
   * @returns {String}       An HTML string
   */
  function template (props) {

    // If there's an error, show it
    if (props.error) {
      return (
        "<p>" +
          "<strong>Sorry, there was a problem. Please try again later.</strong>" +
        "</p>"
      );
    }

    // Otherwise, show the listings
    return (
      "<ul class='places'>" +
        props.places.map(createListing).join("") +
      "</ul>"
    );

  }

  /**
   * Get the JSON data from a Fetch request
   * @param   {Object} response The response to the request
   * @returns {Object}          The JSON data OR a rejected promise
   */
  function getJSON (response) {
    return response.ok ? response.json() : Promise.reject(response);
  }

  /**
   * Set the component's places property
   * @param {Object} data The data from the API
   */
  function setData (data) {
    app.data.places = data;
  }

  /**
   * Set the component's error property
   */
  function setError () {
    app.data.error = true;
  }


  //
  // Inits & Event Listeners
  //

  // Render the component with the API data
  fetch(placesAPI)
    .then(getJSON)
    .then(setData)
    .catch(setError);

})();