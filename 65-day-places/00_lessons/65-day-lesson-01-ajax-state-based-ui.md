# Ajax and state-based UI

In this lesson we look at how to handle state-based UI with asynchronous data that comes from an API.

There are two general approaches:

## 1. Wait for the data before rendering

With this approach, you include some sort of loading message or icon in the HTML.

````html
<div id="app">Loading...</div>
````

So, instead of rendering your component right away, you wait until the API returns data. Once you render the component, it replaces the loading message with actual content.

````javascript
// Create the component
var app = new Rue('#app', {
 data: {},
 template: function (props) {
  return '<ul>' + props.posts.map(function (post) {
   return '<li>' + post.title + '</li>';
  }).join('') + '</ul>';
 }
});

// Fetch data from the API
fetch('https://jsonplaceholder.typicode.com/posts').then(function (response) {
 return response.json();
}).then(function (data) {
 // Reactively update the data
 // This causes the render to happen
 app.data.posts = data;
});

````

## 2. Use your component to create the loading message

With this, second approach, you check for *data* in your component using *Object.keys(props).length*.

The *Object.keys()* creates an array from the keys your *props* object, and the *length* property lets you determine how many there are. If there are none, you know there's no data yet.

````javascript
// Create the component
var app = new Rue('#app', {
 data: {},
 template: function (props) {

  // If there's no data yet
  if (!Object.keys(props).length) {
   return 'Loading...';
  }

  // Once there's data
  return '<ul>' + props.posts.map(function (post) {
   return '<li>' + post.title + '</li>';
  }).join('') + '</ul>';

 }
});

````

Then you render your app **before** making the API call.

````javascript
app.render
````

## Which approach is best

Option two, using the state-based component for the loading message, lets you display a message in the HTML that can be displayed if the JS fails for some reason.

````html
<div id="app">Something went wrong. Please try again.</div>
````

But, this can also result in a flash of unwanted text if the JS file hasn’t loaded yet.

I personally prefer the first option, hard-coding the message, because it loads immediately, before the JS has downloaded and compiled yet. It feels faster even if it’s no more or less performant than the second option.
