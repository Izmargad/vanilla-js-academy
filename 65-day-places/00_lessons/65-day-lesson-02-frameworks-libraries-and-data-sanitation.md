# Frameworks, libraries, and data sanitization

Back when we first worked with APIs, we learned about the importance of sanitizing third party data (data provided by users or from APIs) to help minimize the risk of cross-site script attacks (XSS).

One advantage of using state-based component frameworks and libraries is that most of them have built-in data sanitization.

You use the data as-is, without having to pass it through a *sanitizeHTML()* helper or DOMPurify. The framework or library automatically sanitizes the data (or the resulting HTML) before rendering it into the UI.

React and Vue both do this. Smaller alternatives, like Preact, Svelte, and my own Reef do, too.

I’m not saying, “You should always use a framework.” But a lightweight state-based UI library can make your life a little easier.
