# Textbook Solution from the Academy

## Step 1

For this project, I decided to wait until the API data was loaded to render my state-based component. The first thing I did was add loading text to the #app element.

## Step 2

Next, I created a function to fetch data from the Places API.

Once the API returns data, I assign it to my app.data under the places property. If there’s an error, I’ll set the property to null instead.

This will trigger a reactive UI render, and replace the loading text with my template.

````javascript

/**
 * Get place data from the API and update the app state
 */
var getPlaces = function () {
 fetch('https://vanillajsacademy.com/api/places.json').then(function (response) {
  if (response.ok) {
   return response.json();
  }
  return Promise.reject(response);
 }).then(function (data) {
  app.data.places = data;
 }).catch(function (err) {
  console.warn(err);
  app.data.places = null;
 });
};


````

## Step 3

Next I needed to actually create my state-based component.

Using the *new Reef()* constructor, I created a new *app* component with an empty object as the default *data*. For my *template*, I check to see if *props.places* exists and has any items.

If it does, I’ll pass my *props* into a *getPlacesHTML()* function and return the output. Otherwise, I’ll *return* the output of a *getNoPlacesHTML()* function instead.

````javascript

/**
 * The app component
 */
var app = new Reef('#app', {
 data: {},
 template: function (props) {

  // If there are places, render them
  if (props.places && props.places.length) {
   return getPlacesHTML(props);
  }

  // Otherwise, show an error
  return getNoPlacesHTML();

 }
});


````

In my getNoPlacesHTML() function, I return an HTML string with a message letting people know that there are no places to show at the moment.

````javascript

/**
 * Get the message to display if there's no place data
 * @return {String} The HTML
 */
var getNoPlacesHTML = function () {
 return '<p><em>Unable to find any places right now. Please try again later. Sorry!</em></p>';
};

````

In my *getPlacesHTML()* function, I use the *Array.map()* and *Array.join()* methods to loop through the *props.places* array and create an HTML string for each place.

For each *place*, I included the image, place name, description, location, and URL. I gave each place a *.place* class, and added some wrapper *div* elements I can use for styling.

````javascript

/**
 * Create HTML for each of the places from the app data
 * @param  {Object} props The app data
 * @return {String}       The HTML
 */
var getPlacesHTML = function (props) {
 return props.places.map(function (place) {
  var html =
   '<div class="place">' +
    '<div>' +
     '<img alt="" src="' + place.img + '">' +
    '</div>' +
    '<div>' +
     '<h2>' + place.place + '</h2>' +
     '<p>' + place.description + '</p>' +
     '<p><em>' + place.location + '</em><br><a href="' + place.url + '">' + place.url + '</a></p>' +
    '</div>' +
   '</div>';
  return html;
 }).join('');
};

````

For this project, I used Sarah Drasner’s amazing [CSS Grid Generator](https://cssgrid-generator.netlify.app/) to create a simple grid layout on bigger viewports.

I also added some responsive image styles.

````javascript
/**
 * Grid Layout
 */
@media (min-width: 40em) {
 .place {
  display: grid;
  grid-template-columns: 1fr 2fr;
  grid-template-rows: 1fr;
  grid-column-gap: 2em;
 }
}

/**
 * Responsive Images
 */
img {
 height: auto;
 max-width: 100%;
}
````

## Step 4

With all of my HTML and styles setup, the last thing to do is actually make the API call.

At the very end of my script, I run my *getPlaces()* function to fetch the places data and render my app.

````javascript
//
// Inits
//

getPlaces();

````
