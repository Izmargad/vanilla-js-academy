# Common Issue: Targeting Individual Fields

One approach to this project that works but is not optimal is to target each password field individually with the querySelector() method.

````javascript
// Get the password field and toggle checkbox
var toggle = document.querySelector('#show-passwords');
var currentPW = document.querySelector('#current-password');
var newPW = document.querySelector('#new-password');

// Listen for click events on the toggle
toggle.addEventListener('click', function (event) {

 // If the toggle is checked, change the type to "text"
 // Otherwise, change it back to "password"
 if (toggle.checked) {
  currentPW.type = 'text';
  newPW.type = 'text';
 } else {
  currentPW.type = 'password';
  newPW.type = 'password';
 }

}, false);


````

While this code is functional, it results in you repeating yourself a bit in the code.

It also produces a more fragile end result. If you ever add more password fields or update your IDs, the code will break unless you remember to come back and update things.
