# Project Review: Toggle Password Visibility

I started by using the querySelectorAll() method to get all inputs with a type of password. I passed the returned NodeList into Array.prototype.slice.call() to convert it to an array, and saved it to the passwords variable.

Then, I used querySelector() to get the #show-password checkbox and save it to the toggle variable.I started by using the querySelectorAll() method to get all inputs with a type of password. I passed the returned NodeList into Array.prototype.slice.call() to convert it to an array, and saved it to the passwords variable.

Then, I used querySelector() to get the #show-password checkbox and save it to the toggle variable.

````javascript
// Get the password fields and toggle checkbox
var passwords = Array.prototype.slice.call(document.querySelectorAll('[type="password"]'));
var toggle = document.querySelector('#show-passwords');

````

Next, I attached a click event listener to the toggle element.

````javascript
// Get the password fields and toggle checkbox
var passwords = Array.prototype.slice.call(document.querySelectorAll('[type="password"]'));
var toggle = document.querySelector('#show-passwords');

// Listen for click events on the toggle
toggle.addEventListener('click', function (event) {
 // Do stuff...
}, false);


````

In the callback function, I use the Array.forEach() method to loop through each password field.

For each field, I check to see if the checkbox is checked or not with the checked property.

If it’s checked, I set the type of the password field to text. If it’s not, I set it to password. This will toggle visibility to masked or visible, respectively.

````javascript
// Get the password fields and toggle checkbox
var passwords = Array.prototype.slice.call(document.querySelectorAll('[type="password"]'));
var toggle = document.querySelector('#show-passwords');

// Listen for click events on the toggle
toggle.addEventListener('click', function (event) {

 // Loop through each password field
 passwords.forEach(function (password) {

  // If the toggle is checked, change the type to "text"
  // Otherwise, change it back to "password"
  if (toggle.checked) {
   password.type = 'text';
  } else {
   password.type = 'password';
  }

 });

}, false);



````

And that's the whole script.
