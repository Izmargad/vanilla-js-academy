# Rough Notes from Slack Channel

## From the review Pin

### Question 1: About Polyfills

So, for the polyfill, you have two options:
Include the following script tag from polyfill.io, before your own (CodePen has a special way of doing this which I can explain):
<script crossorigin="anonymous" src="https://polyfill.io/v3/polyfill.min.js?features=default%2CNodeList.prototype.forEach"></script>
Or, you can include the polyfill directly in your code:
<https://vanillajstoolkit.com/polyfills/nodelistforeach/> (edited)

Here is the special area in CodePen where you add the polyfill link (<https://polyfill.io/v3/polyfill.min.js?features=default%2CNodeList.prototype.forEach)> (edited)

Just a quick tought
So as we've seen, we grab the two password fields as a NodeList:
let passwordsNodeList = document.querySelectorAll('[type="password"]');
Then, we transform that into an array:
let passwordsArray = Array.prototype.slice.call(passwordsNodeList );
What do you think about these two other ways of transforming the NodeList into an array?
let passwordsArray = Array.from(passwordsNodeList);
let passwordsArray = [...passwordsNodeList];
I had a look and I see that  Array.prototype.slice.call performs better, but Array.from looks cleaner to me.

I love Array.from(), but it lacks IE support. If you want to use it, I recommend using polyfill.io or adding the polyfill directly to your code:
<https://vanillajstoolkit.com/polyfills/arrayfrom/> (edited)

One other thing about Array.from(), NodeLists, and polyfills…
Since Array.from() has to be polyfilled for IE, I’d personally rather polyfill NodeList.forEach().

### Question 2: Polyfills Privacy Concerns

We had a discussion about that in this thread: <https://vanillajs.slack.com/archives/G011C233HQF/p1589361022072800>
Laura Kalbag
Question about polyfills.io: I have serious privacy (and reliability) concerns about using third-parties for providing core functionality like this. If we’re not happy using a service like this, what would you recommend for polyfills? (Asides from just not writing code that requires polyfills!)

### Random Comments not quite clear

It is more performant to use *event.target* instead of running *querySelector()* again. Because the *event.target* already holds a reference to the element, while querying the DOM again is more work for the browser.

### References worth exploring

* <https://gomakethings.com/listening-for-click-events-with-vanilla-javascript/>
* <https://gomakethings.com/why-event-delegation-is-a-better-way-to-listen-for-events-in-vanilla-js/>
* <https://stackoverflow.com/a/4471462/1944>
* <https://caniuse.com/#feat=let>
* <https://caniuse.com/#feat=const>
* <https://gomakethings.com/how-to-get-started-with-web-development/>
* <https://gomakethings.com/when-should-you-add-the-defer-attribute-to-the-script-element/>

### Question 4

Is there ever a time when a professional developer would put JS at the bottom of HTML?  I always use a separate file.

### Answer 4

Re. question 2, "inlining" your JS into the HTML at the bottom is a relatively old and well used performance tactic. For instance, the venerable YSslow has been recommending this for years. However, there are newer standards we can use such as async and defer:

<https://flaviocopes.com/javascript-async-defer/>

developer.yahoo.comdeveloper.yahoo.com
Best Practices for Speeding Up Your Web Site - Yahoo Developer Network(opens in new tab)

The Exceptional Performance team has identified a number of best practices for making web pages fast.
flaviocopes.comflaviocopes.com

Efficiently load JavaScript with defer and async(opens in new tab)
When loading a script on an HTML page, you need to be careful not to harm the loading performance of the page. Depending on where and how you add your scripts to an HTML page will influence the loading time(46 kB)

* <https://gomakethings.com/inlining-literally-everything-for-better-performance/>
  
### Stuff I now know I don't know

- ternary operators: <https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Conditional_Operator>
- arrow functions

### Event Delegation

* <https://gomakethings.com/wtf-is-use-capture-in-vanilla-js-event-listeners/>
* <https://gomakethings.com/when-do-you-need-to-use-usecapture-with-addeventlistener/>
* <https://gomakethings.com/why-you-shouldnt-just-always-set-usecapture-to-true-with-vanilla-js-event-listeners/>
* <https://gomakethings.com/when-to-use-use-capture-in-your-event-listeners/>
