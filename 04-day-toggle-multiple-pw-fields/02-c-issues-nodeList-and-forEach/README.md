# Common Issue: NodeList and ForEach()

Newer versions of modern browsers provide a NodeList.forEach() method.

````javascript
// Get the password field and toggle checkbox
var passwords = document.querySelectorAll('[type="password"]');
var toggle = document.querySelector('#show-passwords');

// Listen for click events on the toggle
toggle.addEventListener('click', function (event) {

 // Loop through each password field
 passwords.forEach(function (password) {

  // If the toggle is checked, change the type to "text"
  // Otherwise, change it back to "password"
  if (toggle.checked) {
   password.type = 'text';
  } else {
   password.type = 'password';
  }

 });

}, false);


````

However, support is still inconsistent enough that I would recommend either converting the NodeList returned by querySelectorAll() into an array, or adding a [polyfill](https://vanillajstoolkit.com/polyfills/nodelistforeach/) for it.

````javascript
// Get the password field and toggle checkbox
var passwords = Array.prototype.slice.call(document.querySelectorAll('[type="password"]'));
var toggle = document.querySelector('#show-passwords');

// Listen for click events on the toggle
toggle.addEventListener('click', function (event) {

 // Loop through each password field
 passwords.forEach(function (password) {

  // If the toggle is checked, change the type to "text"
  // Otherwise, change it back to "password"
  if (toggle.checked) {
   password.type = 'text';
  } else {
   password.type = 'password';
  }

 });

}, false);



````
