/**
 * 
 * Create a magazine for pirates using an API, 
 * Use localStorage to cache the API data for:
 * - improved performance and 
 * - network resilience.
 * 
 * Call The Scuttlebutt API
 * Return Pirate-themed articles
 * 
 * Use the data to display a list of headlines and
 * Pirate news blurbs
 * 
 * Cache the data for a short time
 * Use that instead of making a fresh API call on subsequent visits / page reloads
 * Make the refresh shorter for the sake of testing in this project
 * 
 */

;(function () {

    // Opt into ES5 strict mode
    "use strict";
  
    //
    // ***** Variables ***** //
    //
      
    // Data the system needs to be aware of:
    // API Key 
    // API URL
    // Elements on the screen (i.e. browser window and document for the DOM)

    // Put your variables here
  
    const app = document.querySelector('#app');
	const endPoint = 'https://vanillajsacademy.com/api/pirates.json';
	const storageID = 'pirateCache';
	const expTime = 1000 * 60 * 2; // expiration time: 2 minutes
    //const expTime = 5000; // valid for 5 seconds

    // End of the bindings (variables) to values
   
    // ***** Helper Methods and Functions *****
  
/*!
	 * Sanitize and encode all HTML in a user-submitted string
	 * (c) 2018 Chris Ferdinandi, MIT License, https://gomakethings.com
	 * @param  {String} str  The user-submitted string
	 * @return {String} str  The sanitized string
	 */
	function sanitizeHTML (str) {
		const temp = document.createElement('div');
		temp.textContent = str;
		return temp.innerHTML;
	}
/**
 * Get the timestamp and check if it is still current
 * Configure the timestamp 
 * (c) 2018 Chris Ferdinandi, MIT License, https://gomakethings.com
 * @param {*} saved 
 * @param {*} goodFor 
 */
    function isDataValid (saved, goodFor) {
		// Check that there's data, and a timestamp key
		if (!saved || !saved.data || !saved.timestamp) return;

		// Get timestamp from cache
        const { timestamp } = JSON.parse(localStorage.getItem(storageID));
        console.log(timestamp);

		// Get the difference between the timestamp and current time
		const difference = new Date().getTime() - timestamp;
		return difference < goodFor;
    }
    
/**
 * The following actions need to be handled and performed
 * ***** Fetch the Pirate News *****
 * Make a call to the endpoint to fetch the news
 * Handle the error if connection is refused
 * 
 * ***** Store the Data Locally *****
 * ***** Render And Display the News *****
 * 
 * ***** Display an Error Message *****
 * If no articles were found display a default error message 
 * Let them link to other pirate news
 */

 // Fetch the news from the API   
 function fetchNews() {
		fetch(endPoint)
			.then(response => {
				if (response.ok) return response.json();
				return Promise.reject(response);
			})
			.then(data => {
				renderAndDisplay(data);
				//fetchNews(data); I love phoning myself ;-)
				saveDataToLocalStorage(data);
			})
			.catch(error => {
				console.log('Something went wrong:', error);
				handleNoArticles();
			});
	}

/**
	 * Cache article data to localStorage
     * Turn the JSON Object into a String
	 * @param  {Object} data The article data
	 */
	const saveDataToLocalStorage = data => {
		// Create a cache object with a timestamp
		const cache = {
			data: data,
			timestamp: new Date().getTime()
		};

		// Stringify it and save it to localStorage
		localStorage.setItem(storageID, JSON.stringify(cache));
	};

// Render and Display the news (data) which had been fetched 
const renderAndDisplay = data => {
    const { articles } = data;

    // If there are no articles, render a message into the UI
    if (articles.length < 1) {
        handleNoArticles();
        return;
    }

    // Otherwise, render the UI
    return (app.innerHTML =
        articles
            .map(
                article =>
                    `<article>
                    <h2>${sanitizeHTML(article.title)}</h2>
                    <p><em>By ${sanitizeHTML(article.author)} on ${sanitizeHTML(
                        article.pubdate
                    )} </em></p>
                    <p>${sanitizeHTML(article.article)}</p>
                </article>`
            )
            .join('') +
        `<p><em>Articles from <a href="${sanitizeHTML(
            data.attribution.url
        )}">${sanitizeHTML(data.attribution.name)}</a></em></p>`)
}

/**
	 * Render a message into the UI when there are no articles to share
	 */
	function handleNoArticles () {
		app.innerHTML =
            '<p>Why Is The Rum Always Gone?</p>' + 
            '<p>Have you heard of  <a href="https://en.wikipedia.org/wiki/Orlando_Pirates_F.C.">Orlando Pirates?</a> from 🇿🇦</p>';
	}
    
  
  
    //
    // Inits & Event Listeners
    //
  
    // Initialize your script here and add any event listeners
  /**
	 * Get API data from localStorage
	 */
	const saved = JSON.parse(localStorage.getItem(storageID))

	if (saved) {
		// Check if it's been less than 2 minutes since the data was saved
		if (isDataValid(saved, expTime)) {
			// The pirate news is kinda fresh from the cache
			// it has been there for expTime 
			console.log('pirate treasure from cache');

			const { data } = saved;
			renderAndDisplay(data);

			return;
		}
    }
    // Get fresh data and use that instead
	console.log('now fetched from API');
	fetchNews();
  })();