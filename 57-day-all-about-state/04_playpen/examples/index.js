function main() {
    const SECOND = 1000;

   const defaultOptions = {
        totalTime: 0,
        template: () => null,
    }

    const Timer = function (selector = '', options = defaultOptions) {
        function Constructor(params) {
            
        }
        this.node = document.querySelector(selector);
        this.totalTime = options.totalTime;
        this.template = options.template;
        this.counterId = null;
        this.counter = this.totalTime;
    }

    Timer.prototype.render = function () {
        this.node.innerHTML = this.template(this.counter);
    }

    Timer.prototype.start = function () {
        this.counterId = window.setInterval(() => {
            this.render();
            console.log(this.counter);
            this.counter = this.counter - 1;
            if (this.counter < 0) {
                window.clearInterval(this.counterId);
                startAgain();
                console.log(this.counter);
            }
        }, SECOND);
        
        this.counter = this.totalTime;
        console.log(this.counter);
    }

    const renderTimer = time => `
        <div class="timer">${time}</div>
    `;

    const app = new Timer('#app', { totalTime: 5, template: renderTimer });
    app.start();

    function startAgain() {
        const button = document.createElement('button');
        const root = document.querySelector('#app');
        button.innerText = 'Start again!';
        button.addEventListener('click', () => app.start());   
        root.appendChild(button);
    }

}

main();