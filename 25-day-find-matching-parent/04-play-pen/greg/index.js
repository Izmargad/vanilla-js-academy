const shuffle = (array = []) => {
    let currentIndex = array.length;

	while (0 !== currentIndex) {
		const randomIndex = Math.floor(Math.random() * currentIndex);
		currentIndex -= 1;
		const temporaryValue = array[currentIndex];
		array[currentIndex] = array[randomIndex];
		array[randomIndex] = temporaryValue;
	}

	return array;
}

function main() {
    const URL = 'https://raw.githubusercontent.com/gist/cferdinandi/3e6b13f1e501e50ce90e92e1f6241258/raw/0891e58c68be46e5d6a6fc8b513ddc016c9d8ebb';
    let root;
    const monsters = [
        'monster1',
        'monster2',
        'monster3',
        'monster4',
        'monster5',
        'monster6',
        'monster7',
        'monster8',
        'monster9',
        'monster10',
        'monster11',
        'sock'
    ];

    const shuffledMonsters = shuffle([...monsters]);

    function selectElements() {
        root = document.querySelector('#app');
    }

    const wrapMonster = monster => `
        <div class="grid"><img src="${URL}/${monster}.svg"></img></div>
    `;

    const prepareFirstCell = element => `<div class="row">${element}`;
    
    const prepareLastCell = element => `${element}</div>`;

    const splitIntoRows = (total, element, index, numOfColumns) => {
        if (index % numOfColumns === 0) {
            return total += prepareFirstCell(element);
        }
        if (index % numOfColumns === numOfColumns - 1) {
            return total += prepareLastCell(element);
        }
        return total += element;
    }

    function createGridOfMonsters(monsters) {
        const COLUMNS = 3;
        const wrappedMonsters = monsters.map(wrapMonster);
        return wrappedMonsters.reduce((all, element, index) => splitIntoRows(all, element, index, COLUMNS), '');
    }

    function renderGrid(grid) {
        root.innerHTML = grid;
    }

    selectElements();
    const gridOfMonsters = createGridOfMonsters(shuffledMonsters);
    renderGrid(gridOfMonsters);
}

main();