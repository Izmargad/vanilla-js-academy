# Common Issue: No alt text

Adding alt text to an image is an easy thing to miss, but not doing so creates a bad experience for users who navigate the web with screen readers.

The alt attribute provides descriptive text that will show up if the image fails to load.

But it’s also read by screen readers. Without one, visually impaired users will not know what the image is.

````javascript
// Create the HTML and inject it into the DOM
app.innerHTML = '<div class="row">' + monsters.map(function (monster) {
 var html =
  '<div class="grid">' +
   '<img src="' + monster + '.svg">' +
  '</div>';
 return html;
}).join('') + '</div>';


````

You should always include it.
