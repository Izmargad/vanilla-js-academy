# Project Review: Monster Shuffle

I started this project by using querySelector() to get the #app element and save it to a variable.

````javascript
// Get the #app element
var app = document.querySelector('#app');

````

I also added the shuffle() helper function.

````javascript
/**
 * Randomly shuffle an array
 * https://stackoverflow.com/a/2450976/1293256
 * @param  {Array} array The array to shuffle
 * @return {String}      The first item in the shuffled array
 */
var shuffle = function (array) {

 var currentIndex = array.length;
 var temporaryValue, randomIndex;

 // While there remain elements to shuffle...
 while (0 !== currentIndex) {
  // Pick a remaining element...
  randomIndex = Math.floor(Math.random() * currentIndex);
  currentIndex -= 1;

  // And swap it with the current element.
  temporaryValue = array[currentIndex];
  array[currentIndex] = array[randomIndex];
  array[randomIndex] = temporaryValue;
 }

 return array;

};


````

Finally, I used the Array.map() method to create an array of HTML strings.

I wrapped each monster in an element with the .grid class. Inside it, I added an image with the monster.svg as the src and the monster name as the alt text.

I used the Array.join() method to combine all of the items into a single string. Then I wrapped it in an element with the .row class, and used the innerHTML property to inject it into the app element.

````javascript
// Create the HTML and inject it into the DOM
app.innerHTML = '<div class="row">' + monsters.map(function (monster) {
 var html =
  '<div class="grid">' +
   '<img alt="' + monster + '" src="' + monster + '.svg">' +
  '</div>';
 return html;
}).join('') + '</div>';



````

And with that, I have a shuffled grid of monsters.
