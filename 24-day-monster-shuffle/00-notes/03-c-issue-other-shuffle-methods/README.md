# Common Issue: Other Shuffle Methods

I sometimes see students use other shuffle methods, typically ones involving Math.random() and Array.sort().

````javascript
/**
 * Randomly shuffle an array
 * https://javascript.info/task/shuffle
 * @param  {Array} array The array to shuffle
 * @return {Array}       The shuffled array
 */
var shuffle = function (array) {
 return array.sort(function () {
  return Math.random() - 0.5;
 });
};



````

While this does shuffle an array, it results in some patterns occuring more than others. [JavaScript.info](https://javascript.info/task/shuffle) explains:

*But because the sorting function is not meant to be used this way, not all permutations have the same probability… We can see the bias clearly: 123 and 213 appear much more often than others.*

And despite being smaller in size, the Fisher-Yates method we looked at it in yesterday’s lesson is actually more efficient for browsers.

It will result in better performance over large data sets.
