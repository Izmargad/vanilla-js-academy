# Day 8: Project Review: Character Count

## Getting an element in the DOM

I started this project by using the ``querySelector()`` method to get the ``#text`` and ``#character-count`` elements. I saved both of them to variables.

```javascript

// Get the text area and character count elements
var text = document.querySelector('#text');
var charCount = document.querySelector('#character-count');


```

Next, I attached an input event listener to the text element. This will fire every time the value of the textarea element changes.

````javascript
// Get the text area and character count elements
var text = document.querySelector('#text');
var charCount = document.querySelector('#character-count');

// Listen for changes to the text area content
text.addEventListener('input', function () {
 // Do something...
}, false);

````

In the event listener callback function, I used the ``value`` and ``length`` properties on the ``text`` element to get the number of characters in the ``textarea``. Then I used the ``textContent`` property to update the value of the ``charCount`` element to that number.

````javascript
// Get the text area and character count elements
var text = document.querySelector('#text');
var charCount = document.querySelector('#character-count');

// Listen for changes to the text area content
text.addEventListener('input', function () {

 // Display the characters count
 charCount.textContent = text.value.length;

}, false);

````

## Browser Compatibility

This works in all modern browsers, and IE9 and above. It can also be used in IE8 with CSS2.1 selectors (no CSS3 support).
