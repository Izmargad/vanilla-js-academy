# Day 8: Common Issue: Unneeded Variables

One habit I see come up a lot is the tendency to set variables when they’re not needed.

In this project, we get the ``length`` of the ``#text`` element’s ``value`` property. Then we use ``textContent`` to update the UI. I often see students first set the ``length`` to a variable, then use that variable to set the ``textContent``.

```javascript

// Get the number of characters in the text area
var length = text.value.length;

// Display the characters count
charCount.textContent = length;



```

This works, but the variable provides no benefit.

It’s only used once. It’s not any more clear to read. It takes up additional space in the browser’s memory (admittedly a comically small amount). It adds more characters to the code base.

We can instead use that value directly when setting the ``textContent``.

````javascript
// Display the characters count
charCount.textContent = text.value.length;
````
