# Day 8: Common Issue: Picking the Right Event

One issue I often see with this project is that people use a less optimal event in their event listener.

Many students use [one of the keyboard](https://developer.mozilla.org/en-US/docs/Web/Events#Keyboard_events) events—like ``keydown``—to detect when users type with their keyboard.

```javascript

// Listen for changes to the text area content
text.addEventListener('keydown', function () {

 // Display the characters count
 charCount.textContent = text.value.length;

}, false);

```

This works, but doesn’t account for users who paste content into the ``textarea``. That would require another event: ``paste``. And what about when someone highlights and cuts text out with their mouse?

One of the thing nice things about the ``input`` event used in the official solution to this project is that it catches any changes to the ``textarea``’s value. That includes both keyboard events *and* text that’s pasted in or cut out.
