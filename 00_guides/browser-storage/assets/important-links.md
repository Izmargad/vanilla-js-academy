
# Important Links

* Student Login - https://courses.gomakethings.com
* Source Code - https://github.com/cferdinandi/data-storage-source-code
* Private Slack Channel - https://gomakethings.com/slack-signup/
* The Vanilla JS Toolkit - https://vanillajstoolkit.com