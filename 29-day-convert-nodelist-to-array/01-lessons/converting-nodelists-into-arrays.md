# Converting NodeLists into Arrays

Methods like *Array.forEach()* and *Array.map()* only work with arrays, not NodeLists (like those returned from *querySelectorAll()*) and other array-like collections. While there is a *NodeList.forEach()* method, it has poor browser support at this time and does not let you use any of the other *Array* methods.

You can convert NodeLists into Arrays by passing them into *Array.prototype.slice.call()*, which will apply the *Array.slice()* method to other array-like objects.

````javascript
var sandwiches = Array.prototype.slice.call(document.querySelectorAll('.sandwich'));
sandwiches.forEach(function (sandwich, index) {
 console.log(sandwich.textContent);
});
````

## Browser Compatibility

Works in all modern browsers, and IE9 and above.
