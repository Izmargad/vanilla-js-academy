        // Your code goes here...
        // I identify the Table of Contents section for the DOM Robot 
        var tocSection = document.querySelector('#table-of-contents');
        // console.log(tocSection);
        // <div id="table-of-contents">

        // now I want to id the h2 elements as an array
        var toc = Array.prototype.slice.call(document.querySelectorAll('[data-toc]'));
        toc.forEach(function (toc, index) {
            // console.log(toc.textContent);
        });
        // only items with the class .sandwich will be listed 

        // Render the Table of Contents
        // Convert the array into HTML
        function display() {
            tocSection.innerHTML = '<ul>' + toc.map(function (heading) {
                return  "<li>" +
                "<a href='#" + heading.id + "'>" + heading.textContent + "</a>" +
              "</li>";
            }).join('') + '</ul>';
        }
        display();