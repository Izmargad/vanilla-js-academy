# Converting NodeLists into Arrays

In today’s project, we’re going to dynamically create a table of contents based on the headings in a document.

## The Template

The document contains an element with the *#table-of-contents* ID. There are also an assortment of heading elements with unique IDs.

Get all of the *h2* elements, create a list of anchor links, and inject it into the *#table-of-contents* element.

````html
<div id="table-of-contents"></div>

<h2 id="cat-o-nine-tails">Cat O'Nine Tails</h2>
<p>Cat o'nine tails Pieces of Eight swab carouser tackle. Pink hornswaggle gabion Sea Legs Davy Jones' Locker.</p>
<p>Hang the jib Nelsons folly trysail ahoy prow. Transom strike colors scallywag aft league.</p>

<h3 id="the-brig">The Brig</h3>
<p>Dead men tell no tales topmast Sail ho Davy Jones' Locker chantey. Wherry fluke pillage rope's end brig.</p>
````

## Browser Compatibility

Works in all modern browsers, and IE9 and above.
