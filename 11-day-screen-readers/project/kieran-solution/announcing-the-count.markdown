Announcing the Count
--------------------
**Use the `aria-live` attribute to announce a live character and word count to screen readers such as VoiceOver on macOS.**

Want to learn how to build this? Join the next session of the [Vanilla JS Academy](https://vanillajsacademy.com/) ⚓️

A [Pen](https://codepen.io/kieranbarker/pen/JjYxoeq) by [Kieran Barker](https://codepen.io/kieranbarker) on [CodePen](https://codepen.io).

[License](https://codepen.io/kieranbarker/pen/JjYxoeq/license).