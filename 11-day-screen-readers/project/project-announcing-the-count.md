# Project: Announcing the Count

In today’s project, we’re going to modify our character and word count project to announce the current count to screen reader users as it changes.

## The Template

We’ll be working with the completed project from yesterday. Changes to the content in ``#word-count`` and ``#character-count`` should be announced to screen reader users.

Test this with an actual screen reader to hear how the announcements are actually made. Do they make sense? Do they provide enough context? What can you do differently if needed to make sure visually impaired users understand what’s happening?

````html

<p>You've written <strong><span id="word-count">0</span> words</strong> and <strong><span id="character-count">0</span> characters</strong>.</p>

````
