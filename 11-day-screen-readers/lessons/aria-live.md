# Aria Live

In our word and character count app, the content in the ``#word-count`` and ``#character-count`` elements change dynamically as the user types. How do people who use screen readers know when that content has changed?

Generally speaking, screen readers will not notice or announce those changes unless you tell them that they need to.

The ``aria-live`` attribute let’s screen readers know that content in a particular element is going to change dynamically, and that they should pay attention to it and announce those changes.

It’s value can be set to ``off`` (the same as not using it at all), ``assertive`` (in which screen readers interrupt user actions to announce changes), and ``polite`` (which tells the screen reader to wait until the user is done to announce updates).

Generally speaking, we should always use ``polite``.

````html
<div id="app" aria-live="polite">The content of this element is going to change.</div>
````
