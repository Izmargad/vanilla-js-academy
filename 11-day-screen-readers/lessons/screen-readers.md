# Screen Readers

Users who are visually impaired may use a piece of software called a screen reader to navigate the web.

Screen readers announce the text on a page out loud so that someone who is visually impaired can use it. They also typically communicate additional information about the page, such as the heading structure and other landmarks.

Both macOS and Windows 10 include free screen reader software. Users on older versions of Windows or Linux have free third-party options to choose from.

## Enabling a screen reader

### On macOS

Open System Preferences, click the Accessibility tab, then click Voice Over. Check the Enable Voiceover box.

### On Windows 10

Click Start, then Settings. Click Ease of Access. Slide the Narrator toggle to On.

### On older Windows devices

You can install [NVDA for free](https://www.nvaccess.org/)

### On Linux

You can install [Orca for free](https://wiki.gnome.org/Projects/Orca)

## Using a screen reader

In order to build accessible web experiences, it’s important to understand how a screen reader announces the content of what you build.

If you’ve never used one before, opening up a site you’ve built, turning on a screen reader, and using the tab key on your keyboard to move through the links or interacting with content on the site can be very enlightening.

Open up your character count app, turn on a screen reader, and type. How do you know what the updated word and character count are?
