 //Your code goes here...
        //Select the checkbox
        var cb = document.querySelector('#show-password');

        // declare a variable to id the textInput field element
        var textInput = document.querySelector('#password');

        // now we listen if the checkbox is clicked
        cb.addEventListener('click', function (event) {
            // if checked - display Text
            // if not - display password hidden text
            if (event.target.checked) {
                console.log("It is in plain sight")
                textInput.type = 'text';
            } else {
                console.log("It is hidden")
                textInput.type = 'password';
            }

        })