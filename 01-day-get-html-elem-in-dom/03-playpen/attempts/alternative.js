 //Your code goes here...

        const checkBox = document.querySelector('#show-password');
        const textInput = document.querySelector('#password');

        function toggleVisibility (){
            if (checkBox.checked) {
                textInput.type = 'text';
            } else {
                 textInput.type = 'password';
            }
        }
        checkBox.addEventListener('click', toggleVisibility);