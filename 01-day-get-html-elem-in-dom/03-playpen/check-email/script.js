 // Variables are declared here
 // I id all the elements the DOM needs to know of
const emailTextInput = document.querySelector('#email');
const textInput = document.querySelector('#password');

const checkBox = document.querySelector('#show-password');
const eCheckBox = document.querySelector('#check-email');

// Methods go here:
// I want to see the password when I check the show password checkbox       
        function toggleVisibility (){
            if (checkBox.checked) {
                textInput.type = 'text';
            } else {
                 textInput.type = 'password';
            }
        }

        // const verifyEmail = (emailTextInput) => emailTextInput.type = 'email'
        // I want the system to check for me if it is a valid email address
        function checkEmailFormat (){
            
                emailTextInput.type = 'email';
          
            }
      
       

// Add our event listeners
// Listens for everything that happens on the page
window.addEventListener('click', toggleVisibility, false);
window.addEventListener('input', checkEmailFormat, false);