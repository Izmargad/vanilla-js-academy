# Playpen experiments

## Toggle Password Visibility

For this project it was great to be able to apply my knowledge and toggle the visibility of the password as well as the validity of the email.

### The Template

When a user clicks the *#check-email* checkbox, to verify that the text input is a valid email, default warning behaviour is called.

````HTML
 <form>
        <div>
            <label for="email">Email</label>
            <input type="text" name="email" id="email">
        </div>

        <div>
            <label for="password">Password</label>
            <input type="password" name="password" id="password">
        </div>

        <div>
            <label for="show-password">
                <input type="checkbox" name="show-passwords" id="show-password">
                Show password
            </label>
        </div>
        <div>
            <label for="check-email">
                <input type="checkbox" name="check-email" id="check-email">
                Verify Email
            </label>
        </div>

        <p>
            <button type="submit">Log In</button>
        </p>

    </form>
````

To check if it is a valid email address, it is dead simple,

````javascript
function checkEmailFormat (){

                emailTextInput.type = 'email';

            }
````
