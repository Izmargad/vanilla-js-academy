# Day 1

## Listening for events and user interactions

Use *addEventListener* to listen for events on and interactions with an element. You can find a full list of available events on the [Mozilla Developer Network](https://developer.mozilla.org/en-US/docs/Web/Events).

Pass in any valid CSS selector. If an element that matches that selector exists in the DOM, or UI, the *querySelector()* method will return the first matching one.

```javascript

var btn = document.querySelector('#click-me');

btn.addEventListener('click', function (event) {
 console.log(event); // The event details
 console.log(event.target); // The clicked element
}, false);


```

## Browser Compatibility

This works in all modern browsers, and IE9 and above.
