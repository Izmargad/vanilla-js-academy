# Day 1

## Getting an element in the DOM

You can use the *document.querySelector()* method to find the first matching element on a page.

Pass in any valid CSS selector. If an element that matches that selector exists in the DOM, or UI, the *querySelector()* method will return the first matching one.

```javascript

// The first div
var elem = document.querySelector('div');

// The first div with the .bg-red class
var elemRed = document.querySelector('.bg-red');

// The first div with a data attribute of snack equal to carrots
var elemCarrots = document.querySelector('[data-snack="carrots"]');

// An element that doesn't exist
var elemNone = document.querySelector('.bg-orange');

```

## Browser Compatibility

This works in all modern browsers, and IE9 and above. It can also be used in IE8 with CSS2.1 selectors (no CSS3 support).
