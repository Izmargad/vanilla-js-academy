# Project Review: Character and Word Count

I started this project by using the ``querySelector()`` method to get the ``#text``, ``#word-count``, and ``#character-count elements``. I saved all of them to variables.

````javascript
// Get the text area, and word and character count elements
var text = document.querySelector('#text');
var wordCount = document.querySelector('#word-count');
var charCount = document.querySelector('#character-count');
````

Next, I attached an ``input`` event listener to the ``text`` element. This will fire every time the value of the ``textarea`` element changes.

````javascript
// Get the text area, and word and character count elements
var text = document.querySelector('#text');
var wordCount = document.querySelector('#word-count');
var charCount = document.querySelector('#character-count');

// Listen for changes to the text area content
text.addEventListener('input', function () {
 // Do something...
}, false);

````

In the event listener callback function, I used the ``split()`` method to convert the ``text.value`` into an array. Then I saved it to the ``words`` variable.

I originally passed in a space (``' '``) as my delimiter, but that does not catch line breaks. Typing a word, hitting enter, and typing some more is treated as one word.

After doing some searches, I found a regex pattern that includes both spaces and line breaks that I used instead.

````javascript
// Get the text area, and word and character count elements
var text = document.querySelector('#text');
var wordCount = document.querySelector('#word-count');
var charCount = document.querySelector('#character-count');

// Listen for changes to the text area content
text.addEventListener('input', function () {

 // Get the word count
 var words = text.value.split(/[\n\r\s]+/g);

}, false);


````

If someone types multiple spaces in a row, the words array will include some items that are just spaces, not words. I used the filter() method to create a new array that contains only words with a length property of greater than zero (0).

I got the number of words using the length property on the words array. Then I used the textContent property on the wordCount element to set its value.

````javascript
// Get the text area, and word and character count elements
var text = document.querySelector('#text');
var wordCount = document.querySelector('#word-count');
var charCount = document.querySelector('#character-count');

// Listen for changes to the text area content
text.addEventListener('input', function () {

 // Get the word count
 var words = text.value.split(/[\n\r\s]+/g).filter(function (word) {
  return word.length > 0;
 });

 // Display the word count
 wordCount.textContent = words.length;

}, false);
````

Finally, I used the value and length properties on the text element to get the number of characters in the textarea. Then I used the textContent property to update the value of the charCount element to that number.

````javascript
// Get the text area, and word and character count elements
var text = document.querySelector('#text');
var wordCount = document.querySelector('#word-count');
var charCount = document.querySelector('#character-count');

// Listen for changes to the text area content
text.addEventListener('input', function () {

 // Get the word count
 var words = text.value.split(/[\n\r\s]+/g).filter(function (word) {
  return word.length > 0;
 });

 // Display the word count
 wordCount.textContent = words.length;

 // Display the characters count
 charCount.textContent = text.value.length;

}, false);`
```
