# Common Issue: Catching Line Breaks

In my original attempt at solving this week’s project, I passed an empty string (' ') into the the String.split() method as my delimiter.

````javascript
// Get the word count
var words = text.value.split(' ');
````

This does give me an array of words, but it fails to capture line breaks. Typing a word, hitting enter, and typing some more is treated as one word.

````javascript
one
word
````

It was actually a student in a previous session of this course who showed me the regular expression pattern (or regex) to use for this.

````javascript
// Get the word count
var words = text.value.split(/[\n\r\s]+/g);
````

That catches spaces (\s) as well as line breaks (\n and \r depending on the operating system). The + catches one or more consecutive characters. And the g at the end is a flag for global, and prevents the regex for stopping after it finds its first match.
