//const endpoint = 'https://api.whatdoestrumpthink.com/api/v1/quotes/random';
const endpoint = 'https://quotesondesign.com/wp-json/posts?filter[orderby]=rand&filter[posts_per_page]=1';

function getQuote() {
  fetch(endpoint)
    .then(function (response) {
      console.log(response);
      return response.json();
      
    })
    .then(function (data) {
      displayQuote(data.message);
      console.log(data);
    })
    .catch(function () {
      console.log("An error occurred");
    });
}

function displayQuote(quote) {
  const quoteText = document.querySelector('.quote-text');
  quoteText.textContent = quote;
}

const newQuoteButton = document.querySelector('.new-quote');
newQuoteButton.addEventListener('click', getQuote);