# Random Ron

Displays a random Ron Swanson quote (from the show *Parks and Recreation*) using the [Ron Swanson Quotes API](https://github.com/jamesseanwright/ron-swanson-quotes).

My third project as part of the [Vanilla JS Academy](https://vanillajsacademy.com/) 😊

[View Demo](https://kieranbarker.github.io/random-ron/)
