;(function(d) {

  //
  // Variables
  //

  var url = "https://ron-swanson-quotes.herokuapp.com/v2/quotes",
      pastQuotes = [],
      app = document.querySelector("#app"),
      announcedContent = document.querySelector(".screen-reader"),
      button = document.querySelector("button");

  //
  // Functions
  //

  function getJSON(response) {
    return (response.ok) ? response.json() : Promise.reject(response);
  }

  // If the quote was among the last fifty quotes, get a new one
  function checkQuote(data) {
    if (pastQuotes.includes(data[0])) {
      return fetch(url).then(getJSON).then(checkQuote);
    }

    if (pastQuotes.length === 50) {
      pastQuotes = [];
    }

    pastQuotes.push(data[0]);

    return Promise.resolve(data[0]);
  }

  function insertQuote(quote) {
    app.innerHTML =
      "<blockquote class='athelas ml0 mt0 pl4 black-90 bl bw2 b--blue'>" +
        "<p class='f5 f4-m f3-l lh-copy measure mt0'>" + quote + "</p>" +
      "</blockquote>";
    announcedContent.textContent = app.textContent;
  }

  function insertError(error) {
    app.innerHTML =
      "<p class='lh-copy measure'>" +
        "Oh no! There was a problem getting the Ron Swanson quote! 😞" +
      "</p>";
    announcedContent.textContent = app.textContent;
  }

  function fetchQuote() {
    fetch(url)
      .then(getJSON)
      .then(checkQuote)
      .then(insertQuote)
      .catch(insertError);
  }

  //
  // Init
  //

  fetchQuote();

  button.addEventListener("click", fetchQuote, false);

})(document);
