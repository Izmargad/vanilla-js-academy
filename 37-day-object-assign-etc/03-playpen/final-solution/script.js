
// IIFE removed and a named function created, by the name of weatherApp

function weatherApp (options) {

    // The Weather App's out of the box settings
    var defaults = {
        key: null,
        selector: '#app',
        units: 'M',
        description: "The Temperature is: {{ temp }}",
        noWeather: 'No weather data available',
        icon: true
    };

    // The user options will overwrite the default values
    var settings = Object.assign(defaults, options);

    // Check for API key and don't run if not provided
    if (!settings.key) {
        console.warn('Please provide an API key.');
        return;
    }

    //
    // Variables
    //

    // Get the #app element
    var app = document.querySelector(settings.selector);

    //
    // Methods
    //

    /**
     * Sanitize and encode all HTML in a user-submitted string
     * @param  {String} str  The user-submitted string
     * @return {String} str  The sanitized string
     */
    var sanitizeHTML = function (str) {
        var temp = document.createElement('div');
        temp.textContent = str;
        return temp.innerHTML;
    };

    

    
    function getIcon (weather) {
        // If the icon is deactivated, return an empty string
        if (!settings.icon) return '';

        // Otherwise, return the icon
        var html =
            "<div class='icon " + weather + "'><img alt='The Weather Icon '" + sanitizeHTML(
                weather
                .weather.description) + " src='https://www.weatherbit.io/static/img/icons/" +
            sanitizeHTML(
                weather.weather.icon) + ".png'></div>";

        return html;
    };

   
    function getDescription (weather) {
        // Return nothing if description is not activated 
        var unit;
        var feelsLike;
        if (!settings.description) return '';

        if (settings.units == 'I') {
            unit = '&deg;F';
           
        } else {
            unit = '&deg;C';
           
        }
        console.log(feelsLike);
        return settings.description
            .replace('{{ temp }}', sanitizeHTML(weather.temp) + unit)
            .replace('{{ feel }}', sanitizeHTML(weather.app_temp));
            
    };

    
    function renderWeather(weather) {

        app.innerHTML =

            "<p>It looks like: " +  "<strong>" + sanitizeHTML(weather.weather.description.toLowerCase()) + "</strong> in " +
            sanitizeHTML(weather.city_name) + "</p>" +
            "<p>Observed at station: " +  "<strong>" + sanitizeHTML(weather.station) + "</p>" +
            getIcon(weather) + getDescription(weather);

    };

    /**
     * No weather data was found, handle error
     */
    function renderNoWeather (error) {
        app.innerHTML = "<p>" + settings.noWeather + "<br /><em>(error: " + error.statusText +
            ")</em></p>";
    };


    //
    // Inits
    //

    // Get the user's location by IP address
    // Then, pass that into the weather API and get the current weather
    fetch('https://ipapi.co/json').then(function (response) {
        if (response.ok) {
            return response.json();
        } else {
            return Promise.reject(response);
        }
    }).then(function (data) {
        // Pass data into another API request
        // Then, return the new Promise into the stream
        return fetch('https://api.weatherbit.io/v2.0/current?key=' + settings.key + '&lat=' +
            data
            .latitude + '&lon=' + data.longitude + '&units=' + settings.units);
    }).then(function (response) {
        if (response.ok) {
            return response.json();
        } else {
            return Promise.reject(response);
        }
    }).then(function (data) {
        // Pass the first weather item into a helper function to render the UI
        renderWeather(data.data[0]);
    }).catch(function (error) {
        renderNoWeather(error);
    });


};

// Change these settings to see the effect
weatherApp({
    key: '6030c62e6d3448ffa355b66dda6b507c', 
    units: 'I',
    icon: true,
    description: "The Temperature is: {{ temp }} <p> Yet, it feels like: {{ feel }}"
});
