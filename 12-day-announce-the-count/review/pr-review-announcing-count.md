# Project Review: Announcing the Count

I originally tried adding aria-live="polite" to #word-count and #character-count elements.

````html
<p>You've written <strong><span id="word-count" aria-live="polite">0</span> words</strong> and <strong><span id="character-count"aria-live="polite">0</span> characters</strong>.</p>
````

While testing with Voice Over on macOS, I noticed that if both the #word-count and #character-count elements updated at the same time, only one of them would be announced.

To make it work the way I wanted, I tried updating my approach.

First, I added an ID of #count and aria-live="polite" to the paragraph element. Then, I removed the IDs and aria-live="polite" from the #word-count and #character-count elements.

````html
<p id="count" aria-live="assertive">You've written <strong>0 words</strong> and <strong>0 characters</strong>.</p>
````

In my JavaScript, I removed the wordCount and charCount variables. Then I used querySelector() to get the #count element and save it to a variable.

````javascript
// Get the text area, and word and character count element
var text = document.querySelector('#text');
var count = document.querySelector('#count');
````

In the event listener, instead of updating the individual elements with textContent, I used innerHTML to update the entire markup inside the count element.

````javascript
// Get the text area, and word and character count element
var text = document.querySelector('#text');
var count = document.querySelector('#count');
````

````javascript
// Listen for changes to the text area content
text.addEventListener('input', function () {

 // Get the word count
 var words = text.value.split(/[\n\r\s]+/g).filter(function (word) {
  return word.length > 0;
 });

 // Display the word and character counts
 count.innerHTML = 'You\'ve written <strong>' + words.length + ' words</strong> and <strong>' + text.value.length + ' characters</strong>.';

}, false);
````

This still only announced the character count update.

Inside an aria-live region, if you have nested elements and the text in one of them changes, but the rest of the text remains the same, it only announces the changed element’s text.

When typing quickly, the character count changes with each key press, while the word count changes less frequently. My screen reader only announces the last change: the character count.

I tried one last thing: removing the markup from the string altogether (and switching to textContent).

````javascript
// Listen for changes to the text area content
text.addEventListener('input', function () {

 // Get the word count
 var words = text.value.split(/[\n\r\s]+/g).filter(function (word) {
  return word.length > 0;
 });

 // Display the word and character counts
 count.textContent = 'You\'ve written ' + words.length + ' words and ' + text.value.length + ' characters.';

}, false);
````

Now, Voice Over announces both updates as I type.
