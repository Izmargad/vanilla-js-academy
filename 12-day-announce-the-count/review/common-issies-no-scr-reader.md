# Common Issues: No Screen Reader

When you know the right things to do to build accessible sites and apps—like using aria-live for dynamic content—it’s tempting to skip testing with actual assistive technology like screen readers.

But they sometimes do not behave the way you would expect.

I had originally added aria-live="polite" to the #word-count and #character-count elements, and figured I was done..

````html
<p>You've written <strong><span id="word-count" aria-live="polite">0</span> words</strong> and <strong><span id="character-count" aria-live="polite">0</span> characters</strong>.</p>
````

In testing the app with Voice Over on macOS in Google Chrome, I noticed that the two seemed to interfere with each other. The #character-count element would announce, but the #word-count element never did.

If two elements were updated at the same time, it was only announcing the last one to be updated.

````javascript
// Display the word count
wordCount.textContent = words.length;

// Display the characters count
charCount.textContent = text.value.length;

````

Switching the order of the wordCount and charCount updates caused #word-count changes to be announced, but #character-count changes to not be when the word count also changed.

````javascript
// Display the characters count
charCount.textContent = text.value.length;

// Display the word count
wordCount.textContent = words.length;

````

Testing with an actual screen reader let me catch an unexpected issue and come up with a more accessible solution.
