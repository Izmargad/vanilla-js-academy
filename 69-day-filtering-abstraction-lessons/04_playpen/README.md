# Project Brief

## User Story

In today’s project, we’re going to modify our places app to let users mark locations they’ve already visited, and filter places by onces they’ve favorited, visited, or haven’t visited.
There are two things you need to do for this project:

* Add a “Visited” button for users to mark off locations they’ve already been to. Like the “Favorites” button, visited locations should be saved between visits.
* When the user selects one of the filters, change the UI to only show locations that match the filter criteria.

## About this template

This is my template for [Vanilla JS Academy](https://vanillajsacademy.com/) projects 🍦

It comes preloaded with:

* A heading and paragraph
* Barebones styles for centering the page
* The default bundle from [polyfill.io](https://polyfill.io/v3/)
* An [immediately-invoked function expression (IIFE)](https://gomakethings.com/the-anatomy-of-an-immediately-invoked-function-expression/)
* A `.gitignore` for Node.js

Feel free to use it as a boilerplate. You can remove anything you don't need 🙂

[**Use This Template &rarr;**](https://github.com/kieranbarker/academy-foundations/generate)
