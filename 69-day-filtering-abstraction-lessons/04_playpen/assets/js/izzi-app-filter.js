//
		// Variables
		//

        var favesID = 'exploreFaves';
		var visitedID = 'exploreVisited';
		
		// This is a binding to a value ID to unexplored places 
		// If it is neither a favorite place or visited place
		var unexploredID = 'unexplored';

		// This is a binding to a value ID to ALL places
		// Visited, Favorites and Unexplored alike
		var allPlacesID = 'allPlaces';
		
		//
		// Methods
		//

		/**
		 * Get all the places from localStorage
		 * @return {Object} Favorite, Visited and Unexplored places
		 */
		var getAllPlaces = function () {
			var allPlaces = localStorage.getItem(allPlacesID);
			var allPlacesObj = allPlaces ? JSON.parse(allPlaces) : {};
			return allPlacesObj;
		};
		
		/**
		 * Get favorite places from localStorage
		 * @return {Object} Favorite places
		 */
		var getFaves = function () {
			var faves = localStorage.getItem(favesID);
			var favesObj = faves ? JSON.parse(faves) : {};
			return favesObj;
		};

		/**
		 * Get unexplored places from localStorage
		 * @return {Object} Unexplored places 
		 */
		var getUnexplored = function () {
			var unexplored = localStorage.getItem(unexploredID);
			var unexploredObj = unexplored ? JSON.parse(unexplored) : {};
			return unexploredObj;
		};
/**
		 * Get visited places from localStorage
		 * @return {Object} Visited places
		 */
		var getVisited = function () {
			var visited = localStorage.getItem(visitedID);
			var visitedObj = visited ? JSON.parse(visited) : {};
			return visitedObj;
		};

		/**
		 * Save unexplored places to localStorage
		 * @param  {Object} faves Unexplored places
		 */
		var saveUnexplored = function (unexplored){
			localStorage.setItem(unexploredID, JSON.stringify(unexplored));
		};

		/**
		 * Save favorite places to localStorage
		 * @param  {Object} faves Favorite places
		 */
		var saveFaves = function (faves) {
			localStorage.setItem(favesID, JSON.stringify(faves));
		};
/**
		 * Save visited places to localStorage
		 * @param  {Object} visited Favorite places
		 */
		var saveVisited = function (visited) {
			localStorage.setItem(visitedID, JSON.stringify(visited));
		};
		/**
		 * Get place data from the API and update the app state
		 */
		var getPlaces = function () {
			fetch('https://vanillajsacademy.com/api/places.json').then(function (response) {
				
				if (response.ok) {
					return response.json();
				}
				return Promise.reject(response);
			}).then(function (data) {
                app.data.faves = getFaves();
				app.data.visited = getVisited();
				// we also need the unexplored places
				app.data.unexplored = getUnexplored();
				// ALL the places
				app.data.places = data;
               

			}).catch(function (err) {
				console.warn(err);
				app.data.places = null;
			});
		};

		/**
		 * Create HTML for each of the places from the app data
		 * @param  {Object} props The app data
		 * @return {String}       The HTML
		 */
		var getPlacesHTML = function (props) {
			return props.places.map(function (place) {
				var html =
					'<div class="place">' +
						'<div>' +
							'<img alt="" src="' + place.img + '">' +
						'</div>' +
						'<div>' +
							'<h2>' + place.place + '</h2>' +
							'<p>' + place.description + '</p>' +
							'<p><em>' + place.location + '</em><br><a href="' + place.url + '">' + place.url + '</a></p>' +
							'<p><button data-fave="' + place.id + '" aria-label="Save ' + place.place + '" aria-pressed="' + props.faves[place.id] + '">&#128154;</button>' +
                            '<button data-visited="' + place.id + '" aria-label="Save ' + place.place + '" aria-pressed="' + props.visited[place.id] + '">&#128204;</button></p>' +
                            '</div>' +
					'</div>';
				return html;
			}).join('');
		};

		/**
		 * Get the message to display if there's no place data
		 * @return {String} The HTML
		 */
		var getNoPlacesHTML = function () {
			return '<p><em>Unable to find any places right now. Please try again later. Sorry!</em></p>';
		};

		/**
		 * The app component
		 */
		var app = new Reef('#app', {
			data: {},
			template: function (props) {

				// If there are places, render them
				if (props.places && props.places.length) {
					return getPlacesHTML(props);
				}

				// Otherwise, show an error
				return getNoPlacesHTML();

			}
		});

		/**
		 * Handle click events
		 * @param  {Event} event The event object
		 */
		var clickHandler = function (event) {

			// Only run on fave buttons
			var place = event.target.getAttribute('data-fave');
			if (!place) return;

			// If place is already saved, remove it
			// Otherwise, save it
			app.data.faves[place] = app.data.faves[place] ? false : true;

			

		};
        var clickVisited = function (event) {

			// Only run on fave buttons
			var visitedPlace = event.target.getAttribute('data-visited');
			if (!visitedPlace) return;

			// If place is already saved, remove it
			// Otherwise, save it
			app.data.visited[visitedPlace] = app.data.visited[visitedPlace] ? false : true;

        };
        // var filterHandler = function (event){
 
        //     const rbs = document.querySelectorAll('input[name="view"]');
        //     let selectedValue;
        //     for (const rb of rbs) {
        //         if (rb.checked) {
        //             selectedValue = rb.value;
        //             break;
        //         }
        //     }
        //     alert(selectedValue);
        //     if (rb.value == 'all'){
        //         console.log("I am supposed to render all the items now")
        //         renderHandler();
               
        //     } else if (rb.value=='faves'){
        //         console.log("Render only favorites");
        //         renderFaves();
        //     }

        //     else if (rb.value=='visited'){
        //         console.log("Render only visited");
        //         renderVisited();
        //     }

        //     else if(rb.value=='not-visited'){
        //         console.log("Render and Display all but the visited")
        //         renderFaves();
        //     }
        //     };
        
        
		/**
		 * Handle render events
		 * @param  {Event} event The event object
		 */
		var renderHandler = function (event) {

			// Save favorites to localStorage on render
            saveFaves(app.data.faves);
            
            // Save visited to localStorage on render
            saveVisited(app.data.visited);

		};

var renderFaves = function(event){
    // only render and display the fave places
    // Save favorites to localStorage on render
    saveFaves(app.data.faves);
    getFaves();
}
var renderVisited = function(event){
    // only render and display the visited places
    saveVisited(app.data.visited);
    getVisited();
}
		//
		// Inits
		//

        getPlaces();
        
        // need to handle event clicking on the filter buttons
        //document.addEventListener('click', filterHandler);
        document.addEventListener('click', clickHandler);
        // below and above should be refactored
        document.addEventListener('click', clickVisited);
		document.addEventListener('render', renderHandler);