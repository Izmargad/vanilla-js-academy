//
		// Variables
		//

		var favesID = 'exploreFaves';


		//
		// Methods
		//

		/**
		 * Get favorite places from localStorage
		 * @return {Object} Favorite places
		 */
		var getFaves = function () {
			var faves = localStorage.getItem(favesID);
			var favesObj = faves ? JSON.parse(faves) : {};
			return favesObj;
		};

		/**
		 * Save favorite places to localStorage
		 * @param  {Object} faves Favorite places
		 */
		var saveFaves = function (faves) {
			localStorage.setItem(favesID, JSON.stringify(faves));
		};

		/**
		 * Get place data from the API and update the app state
		 */
		var getPlaces = function () {
			fetch('https://vanillajsacademy.com/api/places.json').then(function (response) {
				if (response.ok) {
					return response.json();
				}
				return Promise.reject(response);
			}).then(function (data) {
				app.data.faves = getFaves();
				app.data.places = data;
			}).catch(function (err) {
				console.warn(err);
				app.data.places = null;
			});
		};

		/**
		 * Create HTML for each of the places from the app data
		 * @param  {Object} props The app data
		 * @return {String}       The HTML
		 */
		var getPlacesHTML = function (props) {
			return props.places.map(function (place) {
				var html =
					'<div class="place">' +
						'<div>' +
							'<img alt="" src="' + place.img + '">' +
						'</div>' +
						'<div>' +
							'<h2>' + place.place + '</h2>' +
							'<p>' + place.description + '</p>' +
							'<p><em>' + place.location + '</em><br><a href="' + place.url + '">' + place.url + '</a></p>' +
							'<p><button data-fave="' + place.id + '" aria-label="Save ' + place.place + '" aria-pressed="' + props.faves[place.id] + '">♥</button></p>' +
						'</div>' +
					'</div>';
				return html;
			}).join('');
		};

		/**
		 * Get the message to display if there's no place data
		 * @return {String} The HTML
		 */
		var getNoPlacesHTML = function () {
			return '<p><em>Unable to find any places right now. Please try again later. Sorry!</em></p>';
		};

		/**
		 * The app component
		 */
		var app = new Reef('#app', {
			data: {},
			template: function (props) {

				// If there are places, render them
				if (props.places && props.places.length) {
					return getPlacesHTML(props);
				}

				// Otherwise, show an error
				return getNoPlacesHTML();

			}
		});

		/**
		 * Handle click events
		 * @param  {Event} event The event object
		 */
		var clickHandler = function (event) {

			// Only run on fave buttons
			var place = event.target.getAttribute('data-fave');
			if (!place) return;

			// If place is already saved, remove it
			// Otherwise, save it
			app.data.faves[place] = app.data.faves[place] ? false : true;

		};

		/**
		 * Handle render events
		 * @param  {Event} event The event object
		 */
		var renderHandler = function (event) {

			// Save favorites to localStorage on render
			saveFaves(app.data.faves);

		};


		//
		// Inits
		//

		getPlaces();
		document.addEventListener('click', clickHandler);
		document.addEventListener('render', renderHandler);