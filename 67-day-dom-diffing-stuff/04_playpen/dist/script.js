const endpoint = 'https://api.npoint.io/39ce91d0af479ee06a3b';
const COSMIC_STORAGE = 'cosmic-tour-favs';

const getDataFromLS = name => {
	const saved = localStorage.getItem(name);

	return saved ? JSON.parse(saved) : null;
}

const fetchData = url => {
	return fetch(url)
		.then((response) =>
			response.ok ? response.json() : Promise.reject(response)
		);
}

const failComponent = `<div class="failMsg"><p>🛸</p><p>Sorry something went wrong. Please try again later.</p></div>`;

const isFavorite = id => {
	const saved = getDataFromLS(COSMIC_STORAGE);
	return saved && saved.includes(id);
}

const articleComponentBuilder = article => {

	const fav = isFavorite(article.id);

	return `
                <article id="${article.id}">                    
                    <h2>${article.place}</h2>
                    <address>${article.location}</address>
                    <img src="${article.img}" class="full-width"/>
                    <p>${article.description}</p>
                    <button class="fav ${fav ? 'red' : ''}" title="Favorite" aria-label="Favorite" aria-pressed="${fav ? 'true' : 'false'}">&#10084;</button>
                </article>`;
}

const handleErr = err => {
	console.log(err);
	app.innerHTML = failComponent;
}

const toggleFavorite = (e) => {
	if (!e.target.matches('.fav')) return;

	const pressed = (e.target.getAttribute("aria-pressed") === "true");
	const saved = getDataFromLS(COSMIC_STORAGE);

	pressed ? removeFavorite(e.target, saved) : addFavorite(e.target, saved);
}

const addFavorite = (el, saved) => {
	el.classList.toggle('red');
	el.setAttribute("aria-pressed", "true");

	const id = el.parentNode.id;
	const storage = saved || [];
	localStorage.setItem(COSMIC_STORAGE, JSON.stringify([...storage, id]));
}

const removeFavorite = (el, saved) => {

	el.classList.toggle('red');
	el.setAttribute("aria-pressed", "false");

	if (!saved) return;

	const id = el.parentNode.id;
	const filtered = saved.filter(fav => fav != id)
	localStorage.setItem(COSMIC_STORAGE, JSON.stringify(filtered));
}


const main = () => {

	const app = new Reef('#app', {
		data: {},
		template(props) {
			return props.posts.map(articleComponentBuilder).join('');
		}
	});

	fetchData(endpoint)
		.then(data => app.data.posts = data)
		.catch(handleErr);

	document.addEventListener('click', toggleFavorite);
}

main();