# Common Issues: Over-engineering, again

I’ve seen students get creative with how they try to prevent duplicates. One common way people over-engineer this is by using too many approaches to randomize the results.

For example, you may use the ``<count>`` parameter on the Random Ron Swanson Quotes API to get more than one result back.

````javascript
// Get 50 quotes
fetch('http://ron-swanson-quotes.herokuapp.com/v2/quotes/50');

````

Inside the success callback, you might use a technique to randomize or shuffle the array of quotes (the ``data`` array).

````javascript
fetch('http://ron-swanson-quotes.herokuapp.com/v2/quotes/50').then(function (response) {
 if (response.ok) {
  return response.json();
 } else {
  return Promise.reject(response);
 }
}).then(function (data) {

 // Randomize the quotes
 data.sort(function () {
  return Math.random() - 0.5;
 });

});

````

You might then use the ``Array.filter()`` method to get back the first item that’s not already in the ``quotes`` array.

````javascript
fetch('http://ron-swanson-quotes.herokuapp.com/v2/quotes/50').then(function (response) {
 if (response.ok) {
  return response.json();
 } else {
  return Promise.reject(response);
 }
}).then(function (data) {

 // Randomize the quotes
 data.sort(function () {
  return Math.random() - 0.5;
 });

 // Get the first one that's not already been used
 var freshQuote = data.filter(function (quote) {
  return quotes.indexOf(quote) < 0;
 });

 // If the quote is in the quotes array, it's been used already in the last 50 clicks
 // Recursively call getQuote() to fetch another quote instead
 // Then return to quit the function
 if (freshQuote.length < 1) {
  getQuote();
  return;
 }

 // Otherwise, show the quote and add it to the quotes array
 quote.textContent = freshQuote[0];
 quotes.push(freshQuote[0]);

 // If there are 50 items in the quotes array, reset it
 if (quotes.length > 50) {
  quotes = [];
 }

});

````

This works, but like last time, there’s a simpler way to get similar results.

If you find yourself struggling with this a lot, I’d recommend taking a step back and [planning your scripts](https://gomakethings.com/how-to-plan-out-your-javascript-project/) on paper before opening a text editor. It can help clarify what you’re trying to do before you get lost in the code.
