# Project Review: Random Ron without duplicates

In modifying our existing project to prevent duplicates, I started by adding an empty ``quotes`` array.

````javascript
// Create an array to hold used quotes
var quotes = [];
````

In the ``fetch()`` method success handler, after rendering a quote into the UI, I used the ``push()`` method to add it to the ``quotes`` array.

````javascript
// Get a fresh quote and render it into the DOM
var getQuote = function () {
 // Get a Ron Swanson quote
 fetch('http://ron-swanson-quotes.herokuapp.com/v2/quotes').then(function (response) {
  if (response.ok) {
   return response.json();
  } else {
   return Promise.reject(response);
  }
 }).then(function (data) {

  // Show the quote and add it to the quotes array
  quote.textContent = data[0];
  quotes.push(data[0]);

 }).catch(function (error) {
  quote.textContent = '[Something went wrong, sorry!] I have a joke for you... The government in this town is excellent, and uses your tax dollars efficiently.';
 });
};
````

Next, before rendering the quote, I used the ``indexOf()`` method to check if the quote is in the ``quotes`` array already.

If the index is greater than ``-1``, it is. If that’s the case, I call the ``getQuote()`` (calling a function from within itself is called *recursion*) and return to stop the rest of the function from running.

This skips the current quote and fetches a new one.

````javascript
// Get a fresh quote and render it into the DOM
var getQuote = function () {
 // Get a Ron Swanson quote
 fetch('http://ron-swanson-quotes.herokuapp.com/v2/quotes').then(function (response) {
  if (response.ok) {
   return response.json();
  } else {
   return Promise.reject(response);
  }
 }).then(function (data) {

  // If the quote is in the quotes array, it's been used already in the last 50 clicks
  // Recursively call getQuote() to fetch another quote instead
  // Then return to quit the function
  if (quotes.indexOf(data[0]) > -1) {
   getQuote();
   return;
  }

  // Otherwise, show the quote and add it to the quotes array
  quote.textContent = data[0];
  quotes.push(data[0]);

 }).catch(function (error) {
  quote.textContent = '[Something went wrong, sorry!] I have a joke for you... The government in this town is excellent, and uses your tax dollars efficiently.';
 });
};
````

Finally, after rendering the quote into the UI, I check to see if the total number of quotes in the ``quotes`` array is greater than ``50``. If so, I empty the array and start over.

````javascript
// Get a fresh quote and render it into the DOM
var getQuote = function () {
 // Get a Ron Swanson quote
 fetch('http://ron-swanson-quotes.herokuapp.com/v2/quotes').then(function (response) {
  if (response.ok) {
   return response.json();
  } else {
   return Promise.reject(response);
  }
 }).then(function (data) {

  // If the quote is in the quotes array, it's been used already in the last 50 clicks
  // Recursively call getQuote() to fetch another quote instead
  // Then return to quit the function
  if (quotes.indexOf(data[0]) > -1) {
   getQuote();
   return;
  }

  // Otherwise, show the quote and add it to the quotes array
  quote.textContent = data[0];
  quotes.push(data[0]);

  // If there are 50 items in the quotes array, reset it
  if (quotes.length > 50) {
   quotes = [];
  }

 }).catch(function (error) {
  quote.textContent = '[Something went wrong, sorry!] I have a joke for you... The government in this town is excellent, and uses your tax dollars efficiently.';
 });
};
````
