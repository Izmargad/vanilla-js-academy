/**
 * var VS let VS const

var:
  function scoped
  undefined when accessing a variable before it's declared

let:
  block scoped
  ReferenceError when accessing a variable before it's declared

const:
  block scoped
  ReferenceError when accessing a variable before it's declared
  can't be reassigned
 */

// function discountPrices (prices, discount) {
//     var discounted = [];
  
//     for (var i = 0; i < prices.length; i++) {
//       var discountedPrice = prices[i] * (1 - discount)
//       var finalPrice = Math.round(discountedPrice * 100) / 100;
//       discounted.push(finalPrice);
//     }
  
//     console.log(i); 
//     // 3  console.log(discountedPrice) 
//     // 150  console.log(finalPrice) 
//     // 150
//     return discounted;
//   }
  
  // Change the code above that is commented out to:
  // But now, what happens if we change those var declarations 
  // to use let and try to run it?

  function discountPrices (prices, discount) {
    let discounted = []
  
    for (let i = 0; i < prices.length; i++) {
      let discountedPrice = prices[i] * (1 - discount);
      let finalPrice = Math.round(discountedPrice * 100) / 100;
      discounted.push(finalPrice);
    }
  
    console.log(i);
    console.log(discountedPrice);  
    console.log(finalPrice);

    return discounted;
  }
  
  discountPrices([100, 200, 300], .5); // ReferenceError: i is not defined

  // Variables declared with let are block scoped, not function scoped
  // Trying to access i (or discountedPrice or finalPrice)
  // outside of the block
  // in which they were declared 
  // will give a reference error
  