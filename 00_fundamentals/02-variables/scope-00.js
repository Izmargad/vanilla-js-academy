/**
 * SCOPE
 */

/**
 * If the variable statement occurs inside a FunctionDeclaration, 
 * the variables are defined with function-local scope in that function.
 * - ECMAScript Spec
 */

function getDate (){
  // we declare a variable within our getDate() function
  var date = new Date(); 
  return date;
    
  }

  // we invoke the getDate() function
  getDate();
  console.log(date);
  // ReferenceError: date is not defined
  // because date was created inside the getDate() function
  // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Errors/Not_defined
