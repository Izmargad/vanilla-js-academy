/**
 * ES2015 (or ES6) introduced two new ways to create variables, let and const. 
 * But before we actually dive into the differences between var, let, and const, 
 * there are some prerequisites you need to know first. 
 * They are variable declarations vs initialization, 
 * scope (specifically function scope), and hoisting.
 */

// A variable declaration introduces a new identifier.
var declaration;
console.log(declaration); // undefined

 