/**
 * HOISTING
 */

/**
 * 
 * Earlier we said that the definition of hoisting was 
 * “The JavaScript interpreter will assign variable declarations 
 * a default value of undefined during what’s called the 
 * ‘Creation’ phase.” 
 * 
 * We even saw this in action by logging a variable before 
 * it was declared (you get undefined)
 * 
 */


/**
 * Remember earlier we said that “In JavaScript, 
 * variables are initialized with the value of undefined 
 * when they are created.”. 
 * Turns out, that’s all that “Hoisting” is. 
 * The JavaScript interpreter will assign variable declarations 
 * a default value of undefined 
 * during what’s called the “Creation” phase.
 */

function discountPrices (prices, discount) {
  console.log(discounted); // undefined
  var discounted = [];

  for (var i = 0; i < prices.length; i++) {
    var discountedPrice = prices[i] * (1 - discount);
    var finalPrice = Math.round(discountedPrice * 100) / 100;
    discounted.push(finalPrice);
  }

  console.log(i); // 3
  console.log(discountedPrice);// 150
  console.log(finalPrice); // 150

  return discounted;
}

/**
 * I can’t think of any use case where you’d actually want to 
 * access a variable before it was declared. 
 * It seems like throwing a ReferenceError would be a 
 * better default than returning undefined. 
 * In fact, this is exactly what let does. 
 * If you try to access a variable declared with let before it’s declared, 
 * instead of getting undefined (like with those variables declared with var), 
 * you’ll get a ReferenceError.
 */