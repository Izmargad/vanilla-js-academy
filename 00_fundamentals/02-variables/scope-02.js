/**
 * SCOPE
 */

/**
 * 
 * Now let’s look at a more advanced example. 
 * Say we had an array of prices and we needed a function that 
 * took in that array as well as a discount and returned us a 
 * new array of discounted prices. 
 * The end goal might look something like this.
 * 
 * discountPrices ([100,200,300], 5) // [50,100,150] 
 * 
 * And the implementation would look something like this:
 */

function discountPrices (prices, discount) {
  var discounted = [];

  // the variables inside of the for loop 
  // is accessible outside of it
  for (var i = 0; i < prices.length; i++) {
    var discountedPrice = prices[i] * (1 - discount);
    var finalPrice = Math.round(discountedPrice * 100) / 100;
    discounted.push(finalPrice);
  }

  console.log(i); // 3
  console.log(discountedPrice); // 150 
  console.log(finalPrice); // 150

  return discounted;
}

discountPrices ([100,200,300], .5);

/**
 * If JavaScript is the only programming language you know, 
 * you may not think anything of this. 
 * However, if you’re coming to JavaScript from another programming language, 
 * specifically a programming language that is blocked scope, you’re probably 
 * a little bit concerned about what’s going on here. 
 * It’s not really broken, it’s just kind of weird. 
 * There’s not really a reason to still have access to 
 * i, discountedPrice, and finalPrice outside of the for loop. 
 * It doesn’t really do us any good and it may even cause us harm in some cases. 
 * However, since variables declared with var are function scoped, you do.

Now that we’ve discussed variable declarations, initializations, and scope, the last thing we need to flush out before we dive into let and const is hoisting.
 */