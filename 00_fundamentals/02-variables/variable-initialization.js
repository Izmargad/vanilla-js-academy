/**
 * In contrast to *variable declaration*, variable **initialization** 
 * is when you first assign a value to a variable.
 */

// A variable declaration introduces a new identifier.
var declaration;
// So if we log the declaration variable, we get ``undefined``.
console.log(declaration); // undefined

// So here we’re initializing the declaration variable 
// by assigning it to a string.
declaration = 'This is an Initialization';
console.log(declaration); // This is an Initialization