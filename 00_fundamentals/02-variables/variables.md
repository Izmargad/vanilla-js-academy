# Variable Declarations

ES2015 (or ES6) introduced two new ways to create variables, let and const. But before we actually dive into the differences between var, let, and const, there are some prerequisites you need to know first. They are variable declarations vs initialization, scope (specifically function scope), and hoisting.

## Variable Declaration vs Initialization

A variable declaration introduces a new identifier.

````javascript
var declaration
````

Above we create a new identifier called declaration. In JavaScript, variables are initialized with the value of ``undefined`` when they are created. What that means is if we try to log the ``declaration`` variable, we’ll get ``undefined``.

````javascript
var declaration

console.log(declaration) // undefined
````

So if we log the declaration variable, we get ``undefined``.

In contrast to *variable declaration*, variable **initialization** is when you first assign a value to a variable.

````javascript
var declaration

console.log(declaration) // undefined

declaration = 'This is an Initialization'
````

So here we’re initializing the declaration variable by assigning it to a string.

This leads us to our second concept, Scope.

## Scope

Scope defines where variables and functions are accessible inside of your program. In JavaScript, there are two kinds of scope - global scope, and function scope. According to the official spec,

    “If the variable statement occurs inside a FunctionDeclaration, the variables are defined with function-local scope in that function.”.

What that means is if you create a variable with var, that variable is “scoped” to the function it was created in and is only accessible inside of that function or, any nested functions.
