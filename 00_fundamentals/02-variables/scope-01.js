/**
 * SCOPE
 */

/**
 * If the variable statement occurs inside a FunctionDeclaration, 
 * the variables are defined with function-local scope in that function.
 * - ECMAScript Spec
 */

function getDate (){
  // we declare a variable within our getDate() function
  var date = new Date(); 

// now we declare a function inside getDate
// and we call it formatDate()
function formatDate(){
    return date.toDateString().slice(4);

}
// we get the date here
  console.log(date); // it returns today's date 
  return formatDate();
    
  }

  getDate();
  console.log(date);
  // ReferenceError: date is not defined
  // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Errors/Not_defined
