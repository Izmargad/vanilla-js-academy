/**
 * HOISTING
 */

/**
 * Notice all the variable declarations were assigned 
 * a default value of undefined. 
 * That’s why if you try access one of those variables 
 * before it was actually declared,
 * you’ll just get undefined.
 */

function discountPrices (prices, discount) {
    console.log(discounted); // undefined
    var discounted = [];
  
    for (var i = 0; i < prices.length; i++) {
      var discountedPrice = prices[i] * (1 - discount);
      var finalPrice = Math.round(discountedPrice * 100) / 100;
      discounted.push(finalPrice);
    }
  
    console.log(i); // 3
    console.log(discountedPrice); // 150
    console.log(finalPrice); // 150
  
    return discounted;
  }
  