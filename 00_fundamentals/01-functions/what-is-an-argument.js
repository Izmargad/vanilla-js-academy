/**
 * Basic Function Syntax
 * What is an argument
 * 
 * In a function an argument is a value that you can pass into the function
 * When setting your function, you can assign names to the arguments
 * Whatever value you specify will automatically be assigned as a variable
 * A variable which is accessible inside the function
 */

 var add = function (num1, num2) {
     return num1 + num2;
 };
 // num1 and num2 are function arguments

 add(1,3);// returns 4
 