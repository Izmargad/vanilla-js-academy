/**
 * Basic function structure
 * Example of a function, add, which adds to values 
 * It returns the result of the sum of x and y
 * @param {First numeric to use in addition} x 
 * @param {Second numeric value to use in addition} y 
 */

function add (x,y){
    return x + y;
    
  }
  // I press the machine and enter values, say 3 and 5
  const me = add;
  myAnswer = me(3,5);
  console.log('My answer is ', myAnswer)

  // You press the machine and enter values. say 4 and 5
  const you = add;
  yourAnswer = you(4,5)
    console.log('Your answer is ', yourAnswer)
  