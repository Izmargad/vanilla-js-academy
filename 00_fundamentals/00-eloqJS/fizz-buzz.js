function fizzable(n){
    // n is the binding value for a number we loop and run through
    for (let n = 1; n <= 100; n++) {
        // we space it out with an empty string
        let output = "";
        // now we check for remainder on a 3 and 5 value
        // no need for complex if else statements
        if (n % 3 == 0) output += "Fizz";
        if (n % 5 == 0) output += "Buzz";
        // we simply return the binding values
        // concatenation happens automatically
        console.log(output || n);
      }
    }
    // we choose a number binding here
    fizzable(100);