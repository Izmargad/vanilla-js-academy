# Day 3

## Looping over elements

ES5 introduced a new method for loops over arrays: *Array.forEach()*.

You pass a callback function into *forEach()*. The first argument is the current item in the loop. The second is the current index in the array. You can name these two variables anything you want.

Unlike with a *for* loop, you can’t terminate the *forEach()* function before it’s completed. You can *return* to end the current loop (like you would with *continue*in a *for* loop), but you can’t call *break*.

```javascript

var sandwiches = ['turkey', 'tuna', 'ham', 'pb&j'];

// logs 0, tuna, 1, ham, 2, turkey, 3, pb&j
sandwiches.forEach(function (sandwich, index) {
 console.log(index) // index
 console.log(sandwich) // value
});


// Skip "ham"
// logs 0, tuna, 2, turkey, 3, pb&j
sandwiches.forEach(function (sandwich, index) {
 if (sandwich === 'ham') return;
 console.log(index); // index
 console.log(sandwich); // value
});


```

## NodeLists and *Array.forEach()*

The *Array.forEach()* method only works with arrays, not NodeLists (like those returned from *querySelectorAll()*) and other array-like collections. While there is a *NodeList.forEach()* method, it has poor browser support at this time and does not let you use any of the newer *Array* methods.

You can convert NodeLists into Arrays by passing them into *Array.prototype.slice.call()*, which will apply the *Array.slice()* method to other array-like objects.

````javascript
var sandwiches = Array.prototype.slice.call(document.querySelectorAll('.sandwiches'));
sandwiches.forEach(function (sandwich, index) {
 console.log(sandwich.textContent);
});

````

## Browser Compatibility

Both the *Array.forEach()* method and the *Array.prototype.slice.call()* trick are supported in all modern browsers, and IE9 and above.
