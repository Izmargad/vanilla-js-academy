# Day 3

## Getting multiple elements in the DOM

The *document.querySelectorAll()* method returns a NodeList—an array like object—of all matching elements on a page. You can pass in any valid CSS selector as an argument.

```javascript

// Get all elements with the .bg-red class
var elemsRed = document.querySelectorAll('.bg-red');

// Get all elements with the [data-snack] attribute
var elemsSnacks = document.querySelectorAll('[data-snack]');


```

## Browser Compatibility

This works in all modern browsers, and IE9 and above. It can also be used in IE8 with CSS2.1 selectors (no CSS3 support).
