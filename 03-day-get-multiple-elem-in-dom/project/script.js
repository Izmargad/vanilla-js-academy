 //Your code goes here...
 // Get the checkbox toggle and textInputFields for the passwords 
 var checkBox = document.querySelector('#show-passwords');

 // when I get the textInputFields as nodeList I 'cast' it as an array
 var textInputFields = Array.prototype.slice.call(document.querySelectorAll('[type="password"]'));

 // main named function with a callback 
 function toggleVisibility() {
     textInputFields.forEach(function (input) {
         //console.log(input.type, index);

         if (checkBox.checked) {
             input.type = 'text';
         } else {
             input.type = 'password';
         }
     });
 }
 // Listen for click events and call the core function
 checkBox.addEventListener('click', toggleVisibility);