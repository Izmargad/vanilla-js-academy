//
		// Variables
		//

		// Store the weather API key to a variable for easier configuration
		var apiKey = '6030c62e6d3448ffa355b66dda6b507c';

		// Get the #app element
		var app = document.querySelector('#app');
		//var radioButtons = Array.prototype.slice.call(document.querySelectorAll('[type="radio"]'));
		var radioCelsius = document.querySelector('#show-celsius');
		var radioFahrenheit = document.querySelector('#show-fahrenheit');
		var unitOfMeasure;
		console.log(radioCelsius);
		console.log(radioFahrenheit);
		//
		// Methods
		//

		/**
		 * Sanitize and encode all HTML in a user-submitted string
		 * @param  {String} str  The user-submitted string
		 * @return {String} str  The sanitized string
		 */
		function sanitizeHTML(str) {
			var temp = document.createElement('div');
			temp.textContent = str;
			return temp.innerHTML;
		};

		/**
		 * Convert Celsius to Fahrenheit
		 * @param  {String} temp The temperature in celcius
		 * @return {Number}      The temperature in fahrenheit
		 */
		function cToF (temp) {
			return (parseFloat(temp) * 9 / 5) + 32;
		}

		function fToC (temp) {
			return (parseFloat(temp) - 32) * 5/9;
		}
		// determine which radio button is selected
		function setUnit(unit){
			
			if (radioCelsius.checked){
				console.log("Wow Celsius");
				unit = cToF();
			} else if (radioFahrenheit.checked){
			unit = fToC();
				console.log("Wow again Fahrenheit");
				
			}
		}
		/**
		 * Render the weather data into the DOM
		 * @param  {Object} weather The weather data object
		 */
		function renderWeather (weather) {
			app.innerHTML =
			'<p>' + (weather.units) + '</p>' +
				'<p>' +
				'<img src="https://www.weatherbit.io/static/img/icons/' + sanitizeHTML(weather.weather.icon) + '.png">' +
				'</p>' +
				'<p>It is currently <span id="unit">' + cToF(sanitizeHTML(weather.temp)) + '</span> degrees and ' + sanitizeHTML(weather.weather
					.description).toLowerCase() + ' in ' + sanitizeHTML(weather.city_name) + ' at ' + sanitizeHTML(weather.station) +'</p>';
					
				}


		//
		// Inits
		//

		// Get the user's location by IP address
		// Then, pass that into the weather API and get the current weather
		fetch('https://ipapi.co/json').then(function (response) {
			if (response.ok) {
				return response.json();
			} else {
				return Promise.reject(response);
			}
		}).then(function (data) {
			// Pass data into another API request
			// Then, return the new Promise into the stream
			return fetch('https://api.weatherbit.io/v2.0/current?key=' + apiKey + '&lat=' + data.latitude + '&lon=' + 
				data.longitude);
				
		}).then(function (response) {
			if (response.ok) {
				return response.json();
			} else {
				return Promise.reject(response);
			}
		}).then(function (data) {
			// Pass the first weather item into a helper function to render the UI
			renderWeather(data.data[0]);
			console.log(data);
		}).catch(function (error) {
			// Show an error message
			app.textContent = 'Unable to get weather data at this time.';
			console.warn(error);
		});
// Functions

// Create an input handler like this:
function inputHandler (e) {

  var unit = document.querySelector('#unit');
	
  if (e.target.id === 'show-celsius') {
    unit.textContent = fToC(unit.textContent);
  } else if (e.target.id === 'show-fahrenheit') {
    unit.textContent = cToF(unit.textContent);
  }
	
}

window.addEventListener("change", inputHandler);