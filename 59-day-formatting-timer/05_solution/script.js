;(function() {
	'use strict';

	//
	// Variables
	//
	var form = document.querySelector('#save-me');

	//
	// Helper functions
	//

	/**
	 * Add an item to a localStorage() object
	 * @param {String} name  The localStorage() key
	 * @param {String} key   The localStorage() value object key
	 * @param {String} value The localStorage() value object value
	 */
	var addToLocalStorageObject = function(name, key, value) {
		// Get the data that currently exists
		var currentData = localStorage.getItem(name);
		console.log('First log of current data ', currentData);

		// If nothing is there, create an array
		// Else, convert the localStorage string to an array
		// I.e. we parse the strings to a JSON Object Array
		currentData = currentData ? JSON.parse(currentData) : {};

		// Add new data to localStorage Array
		currentData[key] = value;

		// Now we save it back to localStorage, as a string
		localStorage.setItem(name, JSON.stringify(currentData));
		console.log('Second log of current data ', currentData);
	};

	//
	// Methods
	//

	/**
	 * Handle submit events
	 * @param  {Event} event The event object
	 */
	var submitHandler = function(evt) {
		if (!evt.target.closest('form').matches('#save-me')) return;

		if (!localStorage.data) return
		var savedFormData = JSON.parse(localStorage.data);

		for (var key in savedFormData) {
			if (savedFormData.hasOwnProperty(key)) {
				savedFormData[key] = null;
			}
		}

		localStorage.setItem('data', JSON.stringify(savedFormData));
	};

	/**
	 * Handle input events
	 * @param  {Event} event The event object
	 */
	var inputHandler = function(evt) {
		if (!evt.target.closest('#save-me')) return;

		var name = 'data';
		var key = evt.target['name'];
		var value =
			evt.target.type === 'checkbox' ? evt.target.checked : evt.target.value;
		addToLocalStorageObject(name, key, value);
		
		// check that I get the radio button and the checkbox values as checked events
		console.log('Local Storage Object: Here is the name: ', name);
		console.log('Local Storage Object: Here is the key: ', key);
		console.log('Local Storage Object: Here is the value: ', value);
	}



	var loadFormData =  function(_) {
		if (!localStorage.data) return
		var savedFormData = JSON.parse(localStorage.data);

		for (let [key, value] of Object.entries(savedFormData)) {
			if (savedFormData.hasOwnProperty(key)) {
				form[key].value = value;

				if (form[key].type === 'checkbox') {
					form[key].checked = value && true;
				}
			}
		}
	};

	//
	// Inits & Event Listeners
	//

	// Load saved form data from localStorage
	loadFormData();

	// Listen for input events
	document.addEventListener('input', inputHandler, false);

	// Listen for submit events
	document.addEventListener('submit', submitHandler, false);
})();
